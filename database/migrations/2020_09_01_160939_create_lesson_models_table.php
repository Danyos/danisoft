<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->string('slug');
            $table->string('title');
            $table->longText('description');
            $table->longText('language')->nullable();
            $table->string('onlinePersonal');
            $table->string('onlineGroup');
            $table->string('offlinePersonal');
            $table->string('offlineGroup');
            $table->string('math');
            $table->string('time');
            $table->string('avatar');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_models');
    }
}
