<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonPriceListModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_price_list_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('onlinePersonal');
            $table->string('onlineGroup');
            $table->string('offlinePersonal');
            $table->string('offlineGroup');
            $table->string('math');
            $table->string('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_price_list_models');
    }
}
