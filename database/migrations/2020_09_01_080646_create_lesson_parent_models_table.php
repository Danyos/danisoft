<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonParentModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_parent_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('data',['web','design']);
            $table->integer('price_id');
            $table->string('slug');
            $table->string('description');
            $table->string('logo');
            $table->string('avatar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_parent_models');
    }
}
