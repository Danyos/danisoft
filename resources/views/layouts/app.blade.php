<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Danisoft @yield('page_title')</title>
    <meta name="description" content="Մենք զբաղվում ենք կայքերի մշակմամբ նրանց՝ հետագա աջակցությամբ և առաջխաղացմամբ: Մենք հանդես ենք գալիս որպես օղակ հաճախորդի և նրա նոր հաճախորդների միջև՝ հնարավորինս արտացոլելով ընկերության ուշադրությունը կայքերի և դրա նպատակների վրա:"/>
    <meta name="keywords" content="danisoft, danisoft.am, Վեբ կայքեր, կայքերի պատրաստում, վեբ կայքեր, վեբ դասընթացներ,Կայքերի առաջխաղացում, դասընթացներ, website, lesson, dasntacner, kayqer, kayq, smm,"/>
    <meta name="google-site-verification" content="+nxGUDJ4QpAZ5l9Bsjdi102tLVC21AIh5d1Nl23908vVuFHs34="/>
    <meta name="robots" content="noindex,nofollow">

    <meta name="author" content="Daniel Hambardzumyan"/>
    <link rel="icon" href="{{asset('asset/images/danisoft_logo.png')}}">
    <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/style.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('asset/home/home.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/colors/pink')}}.css" id="color-opt"/>
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/contact.css')}}" id="color-opt"/>

    <link href="{{asset('asset/file/main.cdae070e.chunk.css')}}" rel="stylesheet"/>
    <link href="{{asset('asset/css/main.css')}}" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/themify-icons.css')}}"/>
    @yield('css')
<style>



 @keyframes move_wave {
    0% {
        transform: translateX(0) translateZ(0) scaleY(1)
    }
    50% {
        transform: translateX(-25%) translateZ(0) scaleY(0.55)
    }
    100% {
        transform: translateX(-50%) translateZ(0) scaleY(1)
    }
}
.waveWrapper {
    overflow: hidden;
    position: absolute;
    left: 0;
    right: 0;


    margin: auto;
    height: 150px;
}
.waveWrapperInner {
    position: absolute;
    width: 100%;
    overflow: hidden;
    height: 100%;
    bottom: -1px;
    background-image: linear-gradient(to top, #86377b 20%, #27273c 80%);
}
.bgTop {
    z-index: 15;
    opacity: 0.5;
}
.bgMiddle {
    z-index: 10;
    opacity: 0.75;
}
.bgBottom {
    z-index: 5;
}
.wave {
    position: absolute;
    left: 0;
    width: 200%;
    height: 100%;
    background-repeat: repeat no-repeat;
    background-position: 0 bottom;
    transform-origin: center bottom;
}
.waveTop {
    background-size: 50% 100px;
}
.waveAnimation .waveTop {
  animation: move-wave 3s;
   -webkit-animation: move-wave 3s;
   -webkit-animation-delay: 1s;
   animation-delay: 1s;
}
.waveMiddle {
    background-size: 50% 120px;
}
.waveAnimation .waveMiddle {
    animation: move_wave 10s linear infinite;
}
.waveBottom {
    background-size: 50% 100px;
}
.waveAnimation .waveBottom {
    animation: move_wave 15s linear infinite;
}

/*
 *  STYLE 15
 */

</style>
</head>

<body class="bodyHidden" bgcolor="black">

  <div class="scrollbar" id="style-14">
      <div class="force-overflow"></div>
    </div>


@include('include.menu')


@yield('content')

    <section class="section footer">
        <!-- <div class="bg-overlay"></div> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <br>
                        <ul class="list-inline social mb-0">
                            <li class="list-inline-item"><a href="https://facebook.com/danisoft.am" class="social-icon text-muted" target="_blank"><i class="ti-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="https://instagram.com/danisoft.am" class="social-icon text-muted" target="_blank"><i class="ti-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="https://t.me/DanisoftAM" class="social-icon text-muted" style="transform: rotate(89deg);" target="_blank"><i class="ti-location-arrow" s></i></a></li>
                        </ul>
                    </div>
                    <div class="footer-terms">
                        <ul class="mb-0 list-inline text-center mt-4 pt-2">
                            <li class="list-inline-item"><a href="{{route('about')}}" class="text-muted">Մեր մասին</a></li>
                            <li class="list-inline-item"><a href="{{route('lesson')}}" class="text-muted">Դասընթացներ</a></li>
                            <li class="list-inline-item"><a href="{{route('contact')}}" class="text-muted">Կապ</a></li>
                        </ul>
                    </div>

                    <div class="mt-4 pt-2 text-center">
                        <p class="copy-rights text-muted mb-0">DaniSoft ©
                            <script>document.write(new Date().getFullYear())</script>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- Load Facebook SDK for JavaScript -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Your Chat Plugin code -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129525178-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129525178-1');
</script>

    <script src="{{asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{asset('asset/js/popper.min.js')}}"></script>
    <script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
   <script>





       $('.sticky').show();


</script>
@if(session()->has('succsess'))
    <script>
        $(window).scrollTop($('#contact-form').offset().top)
        $(window).scrollTop($('#contact-forms').offset().top)
    </script>
@endif
@if ($errors->has('email') or $errors->has('name') or $errors->has('tel') or $errors->has('message'))

    <script>
        $(window).scrollTop($('#contact-form').offset().top)

    </script>
@endif
    <script type="text/javascript">
        $(document).ready(function () {
            var slides = $('.custom-slide').length;
            var active = 1;
            autoplay();

            function autoplay() {

                $('.active-slide').removeClass('active-slide');
                active = (active == slides) ? 1 : active + 1;
                $('.custom-slide' + active).addClass('active-slide');
                setTimeout(function () {
                    autoplay();
                }, 5000)

            }
        });

        function openHandler(obj) {

            let menu = document.querySelector(obj.dataset.toggle);
            menu.classList.add('menu-opened');

            let wrapper = document.querySelector('.wrapper');
            wrapper.classList.add('wrapper-opened');

            document.body.style.overflow = 'hidden';
        }

        function closeHandler(obj) {
            let menu = document.querySelector(obj.dataset.toggle);
            menu.classList.remove('menu-opened');

            let wrapper = document.querySelector('.wrapper');
            wrapper.classList.remove('wrapper-opened');

            document.body.style.overflow = '';
        }

    </script>

<!-- Load Facebook SDK for JavaScript -->
  <!--    <div id="fb-root"></div>-->
  <!--    <script>-->
  <!--      window.fbAsyncInit = function() {-->
  <!--        FB.init({-->
  <!--          xfbml            : true,-->
  <!--          version          : 'v8.0'-->
  <!--        });-->
  <!--      };-->

  <!--      (function(d, s, id) {-->
  <!--      var js, fjs = d.getElementsByTagName(s)[0];-->
  <!--      if (d.getElementById(id)) return;-->
  <!--      js = d.createElement(s); js.id = id;-->
  <!--      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';-->
  <!--      fjs.parentNode.insertBefore(js, fjs);-->
  <!--    }(document, 'script', 'facebook-jssdk'));</script>-->

      <!-- Your Chat Plugin code -->
  <!--    <div class="fb-customerchat"-->
  <!--      attribution=setup_tool-->
  <!--      page_id="340893473096603"-->
  <!--theme_color="#20cef5"-->
  <!--logged_in_greeting="Բարև Ձեզ, այստեղ կարող եք ուղղել Ձեր հարցը :)"-->
  <!--logged_out_greeting="Բարև Ձեզ, այստեղ կարող եք ուղղել Ձեր հարցը :)">-->
  <!--    </div>-->
@yield('js')

</body>


</html>
