@extends('layouts.app')


@section('content')

    @include('include.slider')



    <div class="ourWork">
        <h1>Ձեր գաղափարը երբեք այնքան լավ տեսք չի ունեցել</h1>
        <hr>
        <h4>DaniSoft-ի կողմից</h4>
        <div class="container22">
            <div class="row">
            <div class="row row2">
                <div class="col-xs-12 col-lg-6 idn">
                    <div>
                        <div class="content content2">
                            <h2>larecat.ml</h2>
                            <h6>Շինարարական</h6>
                            <span><b>lang:</b> HTML, CSS, Bootstrap, jQuery, PHP, Laravel</span>
                            <hr>
                            <p>Պրոֆեսիոնալ Շինարարական տեխնիկայի  կայք որով կարող եք պատվիրել ձեզ հարմար ծառայություները այս կայքի միջոցով</p>
                        </div>
                        <div class="comment">
                            <img src="{{asset('ourwork/vruyr_saribekyan.jpg')}}" alt="vruyr_saribekyan" align="left">
                            <span>"Ես շատ շնորհակալ եմ  ձրի  սարքելու համար"</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 idn">
                    <div class="lastLeft1">
                        <img src="{{asset('ourwork/larecat.ml.png')}}" alt="larecat" align="left">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-6 idn">
                    <div class="lastLeft1">
                        <img src="http://danisoft.am/blog/2020/09/why-have-a-website.png" alt="fastfood" style="height:300px;object-fit: cover;">
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 idn">
                    <div>
                        <div class="content">
                            <h2>fastfood.am</h2>
                            <h6>Խանութ</h6>
                            <span><b>lang:</b> HTML, CSS, Bootstrap, jQuery, PHP, Laravel</span>
                            <hr>
                            <p>Սնդի առաքման կայք որով կարող եք պատվիրել ձեզ հարմար ապրանքը այս կայքի միջոցով</p>
                        </div>
                        <div class="comment">
                            <img src="{{asset('ourwork/vahe_abrahamyan.jpg')}}" alt="person" align="left">

                            <span>"Ես շատ շնորհակալ եմ այս կայքը ստեղծողի անձնակազմից  հիանալի է։ Շնորհակալ եմ ձեզանից"</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row2">
                <div class="col-xs-12 col-lg-6 idn">
                    <div>
                        <div class="content content2">
                            <h2>RBK</h2>
                            <h6>Բանկաին միջնորդ</h6>
                            <span><b></b> HTML, CSS, Bootstrap, jQuery, PHP, Laravel</span>
                            <hr>
                            <p>Եթե ձեզ անհրաժեշտ է սպառոկան վարկ առանց բանկ հաճախելու կարող եք լուծել այդ հարցը</p>                                </div>
                        <div class="comment">
                            <img src="{{asset('ourwork/hakob_movsisyan.jpg')}}" alt="person" align="left" style="object-fit: cover">
                            <span>"Ես շատ շնորհակալ եմ այս կայքը ստեղծողի անձնակազմից  հիանալի համակարգ ստեղցելւ համառ իրոք շնորհակալ եմ աշխատանքի համար"</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 idn">
                    <div class="lastLeft1">
                        <img src="{{asset('ourwork/rbk.am.png')}}" alt="rbk ">                            </div>
                </div>
            </div>

        </div>

    </div>
    </div>


    </div>

@endsection
@section('css')



@endsection
