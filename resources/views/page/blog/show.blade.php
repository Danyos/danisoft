@extends('layouts.app')
@section('page_title', $blogItems->name)
@section('content')
    <div class="heigtbr"></div>

    <div class="page-wrapper">


        <!-- Sidebar Page Container -->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!-- Content Side -->
                    <div class="content-side col-lg-8 col-md-12 col-sm-12">
                        <div class="news-detail">
                            <div class="inner-box">
                                <div class="upper-box">
                                    <h3>{{$blogItems->name}}</h3>

                                </div>
                                <div class="image">
                                    <img src="{{asset($blogItems->avatar)}}" alt="{{$blogItems->slug}}"/>
                                    <div class="post-date">{{$blogItems->created_at->format('d')}}
                                        <span>{{$blogItems->created_at->format(' M ')}}</span></div>
                                </div>

                                <div class="lower-content">
                                    {!! $blogItems->info !!}

                                </div>
                            </div>

                            <div class="comments-area" >
                                <div class="group-title">
                                    <h3> Մեկնաբանություն</h3>
                                </div>
                                @foreach($blogComment as $blogComments)
                                    <div class="comment-box">
                                        <div class="comment">
                                            <div class="author-thumb">
                                                <img src="{{asset($blogComments->avatar)}}" alt="{{$blogComments->name}}">
                                            </div>
                                            <div class="comment-info clearfix"><strong>{{$blogComments->name}}</strong>
                                                <div class="comment-time">{{$blogComments->created_at->format('m-d-Y h:i')}}</div>
                                            </div>
                                            <div class="text">{{$blogComments->message}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <div id="msg"></div>
                            </div>

                            <!-- Comment Form -->
                            <div class="comment-form">

                                <div class="group-title"><h3>Թողնել մեկնաբանություն</h3></div>
                                <!--<div class="alert alert-success d-none" id="msg_div">-->
                                <!--    <span id="res_message">Շնորհակալություն ձեր հայտը ուղղարկվել է</span>-->
                                <!--</div>-->
                                <!--Comment Form-->
                         <div class="fb-comments" data-href="https://danisoft.am" data-numposts="{{$blogItems->id}}" data-width=""></div>

                            </div>


                        </div>
                    </div>

                    <!-- Sidebar Side -->
                    <div class="sidebar-side left-sidebar col-lg-4 col-md-12 col-sm-12">
                        <aside class="sidebar sticky-top">
                            <div class="sidebar-inner">


                                <!-- Categories Widget -->
                                <div class="sidebar-widget categories-widget">
                                    <div class="sidebar-title">
                                        <h5></h5>
                                    </div>
                                    <div class="widget-content">

                                    </div>
                                </div>
                                <div class="heigtbr"></div>
                                <!-- Categories Widget -->
                                <div class="sidebar-widget popular-posts">
                                    <div class="sidebar-title">
                                        <h5>Վերջի նորություները </h5>
                                    </div>
                                    <div class="widget-content">
                                        @foreach($blogall as $blogalls)
                                            <article class="post">
                                                <figure class="post-thumb">
                                                    <img src="{{asset($blogalls->avatar)}}"  alt="{{$blogalls->name}}"  style="height: 60px;object-fit: cover"><a
                                                        href="{{route('blogid',$blogalls->slug)}}"
                                                        class="overlay-box"><span class="icon fa fa-link"></span></a>
                                                </figure>
                                                <div class="text"><a
                                                        href="{{route('blogid',$blogalls->slug)}}">{{$blogalls->name}}</a>
                                                </div>
                                                <div
                                                    class="post-info">{{$blogItems->created_at->format('M d, Y ')}}</div>
                                            </article>
                                        @endforeach
                                    </div>
                                </div>

                            </div>


                            <div class="heigtbr"></div>
                        </aside>
                    </div>

                </div>

            </div>
        </div>
    </div>


    </div>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/blog.css')}}"/>
@endsection
@section('js')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0&appId=2658017531193113&autoLogAppEvents=1" nonce="FPsKPjkE"></script>
  @include('include.blog')
@endsection
