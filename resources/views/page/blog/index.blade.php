@extends('layouts.app')
@section('content')

    <div class="page-wrapper">


        <section class="page-title">
            <div class="pattern-layer-one" style="background-image: url(asset/images/background/pattern-16.png)"></div>
            <div class="auto-container">
                <h2>Նորություներ</h2>

            </div>
        </section>

       <div class="works">
    <div class="worksContent1">
        <h1>Պարզ գործընթաց՝ մինչև գործարկել</h1>
        <div class="w-c-d">
            <div class="row">
                @foreach($blogItems as $blog )
                <div class="cardsItem1 col-xs-10 col-sm-10 col-md-4 " style="padding-top: 20px" onclick="location.href='{{route('blogid',$blog->slug)}}'">
                    <div>
                        <img
                            src="{{asset($blog->avatar)}}" alt="{{$blog->slug}}" style="height: 300px;object-fit: cover">
                        <button class="btn">
                            <i class="ti-link"></i>
                        </button>
                        
                
                         <ul class="newname">
                                <li >
                                    <span class="step">{{$blog->name}}</span>
                                </li>
                             
                            </ul>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
    
    <div class="" align="center" style="width: 50%;height:100px;">
               
           
            
                  <div class="col col-xs-4">
                       {{$blogItems->links()}} 
                  </div>
      
        
 

    </div>
</div>

</div>


@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/blog.css')}}" />
    <style>
    .pagination{
           margin: 30px auto;
    position: absolute;
    right: 0px;
    }
     
    </style>
@endsection
