@extends('layouts.app')

@section('content')
    <div class="heigtbr"></div>


    <div class="aboutUs_About__oPDNP"><h1>Մենք և մեր ծառայություները</h1>
        <hr>
        <h6>Մենք միշտ ենք երջանիկ համար նորը</h6></div>
    <br>

    @if(session()->has('succsess'))
        <span class="help-block">
                        <p class="alert alert-success" align="center">
                <strong>Հաստատված!</strong> Մեր մասնագետները կկապվեն ձեզ հետ.
                        </p>
                       </span>
    @endif
    @if ($errors->has('tel') or $errors->has('name')or $errors->has('package')or $errors->has('parent_id'))
        <span class="help-block">
                        <p class="alert alert-danger" align="center">
                <strong>Սխալ!</strong> Խնդրումենք ճիշտ լրացնել դաշտերը.
                        </p>
                       </span>
    @endif
    <div class="container-fluid product_page">
        <div class="container">

            <div class="row">
                <div class="col-xl-4 col-md-12 col-lg-4 order-1 order-lg-0 ">
                    <div class="p_page_list_window  d-lg-block">
                        <ul class="navbar-nav cat_list">
                            @foreach($service as $k=>$services)
                                @if($services->slug==$service_id)
                                    <li class="nav-item"><a href="{{route('service',$services->slug)}}"
                                                            class="inActive">{{$services->title}}</a></li>
                                @else
                                    <li class="nav-item"><a href="{{route('service',$services->slug)}}"
                                                            class="">{{$services->title}}</a></li>
                                @endif
                            @endforeach
                        </ul>


                    </div>
                </div>

                <div class="col-xl-8 col-md-12 col-lg-8 order-0 order-lg-1">
                    <div class="p_page_product-window">
                        <img src="{{asset($lastservice->avatar)}}" class="img-fluid" alt="{{$lastservice->title}}">
                        <h3 class="services-right-title">    {{$lastservice->title}}</h3>
                        <div class="row">
                            <div class="col-12 services-right-text">
                                <p>
                                    {!! $lastservice->description !!}
                                </p>

                            </div>


                            <div class="container">
                                @if($lastservice->slug!='education')
                                    <div class="row ">
                                        @foreach($service_informate as $service_informates)

                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 ">
                                                <div class="price-table pt-bg-blue ">
                                                    <div class="fgrerfe">
                                                        <h4>{{$service_informates->title}}</h4>
                                                        <br>
                                                        <img src="{{$service_informates->avatar}}" alt="">

                                                        <span> {{$service_informates->price}} </span>


                                                    </div>
                                                    <ul class="price-colums removes{{$service_informates->id}}">
                                                        @foreach($service_informates->serviceMain->take(2) as $serviceMain)
                                                            @if($serviceMain->title)
                                                                <li>{{$serviceMain->title}}</li>
                                                            @endif
                                                            @if($serviceMain->description)
                                                                <li>
                                                                    <i class="fa fa-check"></i> {!! $serviceMain->description !!}
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                    <p class="lastp">
                                                        <button class="remove-price-colum " data-toggle="modal"
                                                                data-target="#v{{$service_informates->id}}">Ավելին
                                                        </button>
                                                    </p>
                                                    <a data-toggle="modal" data-target="#d{{$service_informates->id}}">Պատվիրել</a>
                                                </div>
                                            </div>

                                            <div class="modal fade" id="d{{$service_informates->id}}" role="dialog"
                                                 style="margin-top: 48px">
                                                <!-- modal dialog -->
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- modal header -->
                                                        <div class="modal-header">
                                                            <div class="mohe">
                                                                <h4 class="modal-title">{{$service_informates->title}}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-success d-none" id="msg_div">
                                                            <span id="res_message"></span>
                                                        </div>
                                                        <div class="modal-body">


                                                            <fieldset>
                                                                <form id="contact_us" method="post"
                                                                      action="{{route('package')}}"
                                                                      class="registration-form contact_us ">
                                                                    @csrf
                                                                    <label class="text-muted"
                                                                           for="InputName1">Անուն</label>
                                                                    <input type="text" name="name" id="InputName1"
                                                                           value="{{ old('name') }}" required
                                                                           class="form-control mb-2 registration-input-box">
                                                                    <span
                                                                        class="text-danger">{{ $errors->first('name') }}</span>
                                                                    <label class="text-muted" for="InputTel">Հեռախոսահամար</label>
                                                                    <input type="number" name="tel" id="InputTel"
                                                                           value="{{ old('tel') }}" required
                                                                           class="form-control mb-2 registration-input-box">
                                                                    <input type="hidden"
                                                                           value="{{$service_informates->title}}"
                                                                           name="package">
                                                                    <input type="hidden"
                                                                           value="{{$service_informates->id}}"
                                                                           name="parent_id">
                                                                    <span
                                                                        class="text-danger">{{ $errors->first('tel') }}</span>
                                                                    <button
                                                                        class="btn btn-custom w-100 mt-3 text-uppercase"
                                                                        type="submit" id="send_form">Հաստատել
                                                                    </button>
                                                                </form>

                                                            </fieldset>

                                                        </div>
                                                        <!-- end modal body -->
                                                        <!-- modal footer-->
                                                        <div class="modal-footer">
                                                            <div class="mohe">
                                                                <button class="btn btn-default btn-md btn-sm hidemodal"
                                                                        data-dismiss="modal"> Փակել
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- end modal footer -->
                                                    </div>
                                                    <!-- end modal content-->
                                                </div>
                                                <!-- end modal dialog-->
                                            </div>
                                            <div class="modal fade" id="v{{$service_informates->id}}" role="dialog"
                                                 style="margin-top: 48px">
                                                <!-- modal dialog -->
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- modal header -->
                                                        <div class="modal-header">
                                                            <div class="mohe">
                                                                <h4 class="modal-title">{{$service_informates->title}}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-success d-none" id="msg_div">
                                                            <span id="res_message"></span>
                                                        </div>
                                                        <div class="modal-body">


                                                            <ul class="price-colums removes{{$service_informates->id}}">
                                                                @foreach($service_informates->serviceMain as $serviceMain)
                                                                    @if($serviceMain->title)
                                                                        <li>{{$serviceMain->title}}</li>
                                                                    @endif
                                                                    @if($serviceMain->description)
                                                                        <li>
                                                                            <i class="fa fa-check"></i> {!! $serviceMain->description !!}
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            </ul>


                                                        </div>
                                                        <!-- end modal body -->
                                                        <!-- modal footer-->
                                                        <div class="modal-footer">
                                                            <div class="mohe">
                                                                <button class="btn btn-default btn-md btn-sm hidemodal"
                                                                        data-dismiss="modal"> Փակել
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- end modal footer -->
                                                    </div>
                                                    <!-- end modal content-->
                                                </div>
                                                <!-- end modal dialog-->
                                            </div>



                                        @endforeach
                                    </div>
                                @else

                                    <div class="row ">
                                        @foreach($service_informate as $lessonInformates)
                                            <div class="col-12 col-sm-6 ">
                                                <div class="wrapers">

                                                    <p class="lastp">
                                                        <button
                                                            onclick="location.href='{{$lessonInformates->href}}'">{{$lessonInformates->title}}</button>
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <hr>
                            <br>
                            <div>
                                <p class="text-left" style="margin:36px;">
                                    {!! $lastservice->offer !!}

                                </p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/service.css')}}"/>

@endsection


@section('js')


@endsection
