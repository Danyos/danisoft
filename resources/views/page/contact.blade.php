@extends('layouts.app')
@section('content')
@include('include.slider')

    <div>


        <div class="container">

            <div class="row">
                <div class="col-12" align="center"  id="contact-forms">


                   <div class="aboutUs_About__oPDNP"><h1>Եկ <span class="font-weight-bold">Խոսենք</span></h1>
                        <hr>
                       <div class="content">

                           <h1 class="typedtext"></h1>


                       </div>
                   </div>

                </div>
            </div>

            <div class="contact-parent">
                <div class="contact-child child1" >
                    <p>
                        <i class="ti-shift-right"></i> Հասցե <br/>
                        <span> Ք․ Երևան
                                <br/>
                                Մարշալ Բաղրամյան
                            </span>
                    </p>

                    <p>
                        <i class="ti-mobile"></i> Հեռախոսահամար <br/>
                        <span> +374(98)65-75-45</span>
                    </p>

                    <p>
                        <i class="ti-email"></i> Փոստ <br/>
                        <span>info@danisoft.am</span>
                    </p>
                </div>

                <div class="contact-child child2">
                    <form class="inside-contact" action="{{route('send')}}" method="post">
                        @csrf

                    @if(session()->has('succsess'))
                            <span class="help-block">
                        <p class="alert alert-success" align="center">
                <strong>Հաստատված!</strong> Մեր մասնագետները կկապվեն ձեզ հետ.
                        </p>
                       </span>
                        @endif
                        <br>

                        <p>Անուն *</p>
                        <input id="txt_name" type="text" Required="required" name="name" value="{{old('name')}}" >
                        @if ($errors->has('name'))
                            <br>
                            <span class="help-block">
                       </span>
                            <p class="alert alert-danger"> <strong>Սխալ!</strong> Փորձեք ճիշտ լրացնել դաշտերը.  </p>
                        @endif

                        <p>Փոստ *</p>
                        <input id="txt_email" type="email" Required="required" name="email" value="{{old('email')}}" required>
                        @if ($errors->has('email'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                        </p>
                <strong>Սխալ!</strong> Փորձեք ճիշտ լրացնել դաշտերը
                       </span>
                        @endif

                        <p>Հեռախոսահամար *</p>
                        <input id="txt_phone" type="text" Required="required" name="tel" value="{{old('tel')}}" required>
                        @if ($errors->has('tel'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                        </p>
                <strong>Սխալ!</strong>Փորձեք ճիշտ լրացնել դաշտերը.
                       </span>
                        @endif

                        <p>Բովանդակություն *</p>
                        <textarea id="txt_message" rows="4" cols="20" Required="required"name="message" >{{old('message')}}</textarea>
                        @if ($errors->has('message'))
                            <br>
                            <span class="help-block">
                        <p class="alert alert-danger">
                        </p>
                <strong>Սխալ!</strong> Փորձեք ճիշտ լրացնել դաշտերը.
                       </span>
                        @endif

                        <input type="submit" id="btn_send" value="Ուղարկել">
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="heigtbr"></div>
@endsection

@section('js')
    @if(session()->has('succsess'))
        <script>

            $(window).scrollTop($('#contact-forms').offset().top)
        </script>
    @endif
    <script type="text/javascript">
        function typingEffect() {
            const contactTexts = shuffleArray(['Ինչ հարցեր ունեք?😊', 'Հետաքրքիր էր արդոք նյութերը!😄', 'Կցանկանայիք դուք էլ լինել ծրագրավորող?', 'Կապվիր մեզ հետ?👍']);
            const typedtext = document.getElementsByClassName("typedtext")[0];
            let removing = false;
            let idx = char = 0;

            setInterval(() => { // We define the interval of the typing speed

                // If we do not reach the limit, we insert characters in the html
                if (char < contactTexts[idx].length) typedtext.innerHTML += contactTexts[idx][char];

                // 15*150ms = time before starting to remove characters
                if (char == contactTexts[idx].length + 15) removing = true;

                // Removing characters, the last one always
                if (removing) typedtext.innerHTML = typedtext.innerHTML.substring(0, typedtext.innerHTML.length - 1);

                char++; // Next character

                // When there is nothing else to remove
                if (typedtext.innerHTML.length === 0) {

                    // If we get to the end of the texts we start over
                    if (idx === contactTexts.length - 1) idx = 0
                    else idx++;

                    char = 0; // Start the next text by the first character
                    removing = false; // No more removing characters
                }

            }, 150); // Typing speed, 150 ms

        }
        typingEffect();
        function shuffleArray(array) {
            let currentIndex = array.length,
                temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

    </script>
@endsection
