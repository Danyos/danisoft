@extends('layouts.app')


@section('content')

@include('include.slider')
    <div class="aboutUs_About__oPDNP"><h1>Մենք և մեր գործունեությունը</h1>
        <hr>
        <h6>Մենք միշտ ենք երջանիկ համար նորը</h6>
        <div class="whoWeAre_mainContainer__1E5fL">
            <div class="MuiContainer-root MuiContainer-maxWidthMd">
                <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2">
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6">
                        <div class="left_Left__3N5P0"><h3>  {{$aboutParent->title}}</h3>
                            <div class="left_textContent__9yjOs">
                                {{$aboutParent->description}}
                            </div>
                        </div>
                    </div>
                    <div class="MuiGrid-root right_rPanel__1kCl7 MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6">
                        <img class="right_img__HXEnI"
                             src="{{asset('asset/file/about_Who_were_are_6wP3o3oM5aL2l3QFJbM0askaZy.png')}}"
                             alt="whoweare"></div>
                </div>
            </div>
        </div>
        <div class="ourValues_mainContainer__3RMHQ">
            <div class="MuiContainer-root MuiContainer-maxWidthMd">
                <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2">
                    <div class="MuiGrid-root left_imgContent__2t-X- MuiGrid-item MuiGrid-grid-sm-6">
                        <img src="{{asset('asset/file/about_mission_v6L6oIcy8KYrrlbijkcld7Ryr9.png')}}"
                             class="left_uplImage__J87Zp" alt="top"></div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6">

                            @foreach($aboutinformate as $aboutinformates)
                            <h5> {{$aboutinformates->title}}</h5>
                            <p>{{$aboutinformates->description}}</p>
                            @endforeach
                         </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection
@section('css')



@endsection
