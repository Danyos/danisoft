@extends('layouts.app')


@section('content')

    <div id="root">
        <div class="lesson_Lessons__583Aq"><h1></h1>

            <div class="paralax_Paralax__CvPMY"
                 style="background-image: url({{asset('asset/img/header_programmer.jpg')}}); height: 200px;   display: flex;
                     align-items: flex-end;">
                <h2 style="text-align: center; align-self: center;
    flex: 1;">Հատուկ դասընթացներ</h2></div>
            <div class="MuiContainer-root MuiContainer-maxWidthLg">
                <br>
                <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2">
                    @foreach($lesson as $lessons)
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6 MuiGrid-grid-md-4">
                            <div class="item_lesson__1fwSW">
                                <div class="item_imageWindow__qHj49">
                                    <img src="{{asset($lessons->logo)}}" alt="{{$lessons->name}}"></div>
                                <h4>{{$lessons->name}}</h4>
                                <div class="item_textContent__3lK6M"><p class="item_description__35N0y"><b style="text-align: center;display: block">Նկարագրություն <br></b>

                                        <span>

                                            {{ mb_strlen( strip_tags($lessons->description, '<b><i><u>') ) > 250 ? mb_substr(strip_tags($lessons->description, '<b><i><u>'), 0, 250) . ' ...' : strip_tags($lessons->description, '<b><i><u>') }}

                                        </span>
                                    </p></div>
                                <p class="item_link__3xlcX">
                                    <button class="button_Button__3j--r" onclick="location.href='{{route('lesson.show',$lessons->slug)}}'">Իմանալ Ավելին</button>
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>


@endsection

