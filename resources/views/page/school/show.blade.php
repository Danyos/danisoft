@extends('layouts.app')


@section('content')
    <div id="root">
        <div class="view_ViewLesson__3W7TW">
            <div class="MuiContainer-root MuiContainer-maxWidthLg">
                <header class="header_viewLessonHeader__1t5gT">
                    <h1>{{$lesson->name}}</h1>
                    <div class="header_secondHeaderText__36eeC">{{$lesson->slogan}}</div>

                </header>
            </div>
            <div class="info_LessonInfo__3tp6-">
                <div class="MuiContainer-root MuiContainer-maxWidthLg">
                    <ul>
                        <li>
                            <img src="{{asset('asset/lesson/stopwatch.svg')}}" alt="{{$lesson->slogan}}" class="MuiSvgIcon-root">
                            <span>{{$pricedata->time}} րոպե</span></li>
                        <li>
                            <img src="{{asset('asset/lesson/calendar.svg')}}" alt="{{$lesson->slogan}}" class="MuiSvgIcon-root">
                            <span>{{$pricedata->math}} ամիս</span></li>
                        <li>

                            <input type="checkbox" class="MuiSvgIcon-root" id="chekedlessonOffline" value="" onchange="offline('offline')" style="display: none" checked>

                            <input type="checkbox" class="MuiSvgIcon-root" id="chekedlesson" value="" onchange="offline('online')" checked>
                           <span><b id="msg">Online</b></span></li>
                        <li>
                            <img src="{{asset('asset/lesson/person.svg')}}" alt="Online Lesson" class="MuiSvgIcon-root">
                            <span><b id="onlineinvalid">{{$pricedata->onlinePersonal}}</b><small> դրամ</small></span></li>
                        <li>

                            <img src="{{asset('asset/lesson/group.svg')}}" alt="group lesson" class="MuiSvgIcon-root " >
                            <span><b id="onlinegroupe">{{$pricedata->onlineGroup}}</b><small> դրամ</small></span></li>
                    </ul>
                </div>
            </div>
            <div class="MuiContainer-root MuiContainer-maxWidthLg">
                <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2">
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-8 MuiGrid-grid-md-9">


                        <div class="p_page_product-window">
                            <img src="{{asset($lesson->avatar)}}" class="img-fluid classavatar" alt="{{$lesson->description}}">
                            <h3 class="services-right-title">Հիմնական Մաս</h3>
                            <div class="row">
                                <div class="col-12 services-right-text">
                                    <p>

                                        {!! $lesson->description !!}


                                    </p>
                                </div>
                                <div class="row">
                                    @foreach($lessonInformate as $lessonInformates)
                                        <div class="col-lg-6 col-md-12">
                                            <div class="wrappers" style="width: 100%;  margin: 25px auto;">
                                                <p class="baners" >{{$lessonInformates->title}}</p>
                                                <p class=" b-l-2" style="width:80%;margin-left: 23px;">
                                                    {{ $lessonInformates->description }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <hr>


                        </div>


                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-4 MuiGrid-grid-md-3">

                        <div class="aside_trainers__2QrDN">
                            <h5 align="center">Դասավանդող ուսուցիչներ </h5>
                            @foreach($trainers as $trainerss)
                            <div class="item_item__1tpTK" style="width: 90%;">
                                <div class="item_imageWindow__1DLD5">
                                    <img src="{{asset($trainerss->avatar)}}" alt="{{$trainerss->name}}">
                                </div>
                                <div class="item_contentWindow__3_RMr">
                                    <h5>{{$trainerss->name}}</h5>
                                    <p>{{$trainerss->profession}}</p>

                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="aside_knewladge__1kAGj" style="background: red">

                            <ul>
                                <li>Առաջին դասը փորձնական անվճար է</li>
                                <li>Արժեքը նշված է 1 ամսվա համար</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h1>Գրանցվել դասընթացին</h1>
        <form  id="contact_us" method="post" action="javascript:void(0)">
            @csrf
            <div>
                <p class="label username">Անուն Ազգանուն</p>
                <input type="text" name="name" id="formGroupExampleInput" >

                <span class="text-danger">{{ $errors->first('name') }}</span>

            </div>
            <div>
                <p class="label password">Տարիք</p>
                <input type="number" name="age">
                <span class="text-danger">{{ $errors->first('age') }}</span>
            </div>
            <div>
                <span class="text-danger">{{ $errors->first('tel') }}</span>
                <p class="label confirm-password">Հեռախոսահամար</p>
                <input type="tel" name="tel" >


            </div>
            <div>
                <p class="label email">Փոստ</p>
                <input type="email" name="email" id="email" >

                <span class="text-danger">{{ $errors->first('email') }}</span>

            </div>

            <input type="hidden" name="slug" value="{{$lesson->name}}" >

            <div class="alert alert-success d-none" id="msg_div">

                <span id="res_message"></span>

            </div>

                <button class="btn btn-custom w-100 mt-3 text-uppercase" id="send_form">Գրանցվել</button>


        </form>
    </div>
@endsection
@section('css')

<style>
    .classavatar{
        height: 400px;
        object-fit: cover;
    }
    form{
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 10px 30px;
    }

    h1{
        text-align: center;
        letter-spacing: 2px;
    }

    form div{
        display: flex;
        flex-direction: column;
        width: 100%;
    }

    .label, form div input{
        font-size: 1.3rem;
    }

    .label {
        transform: translateY(100%) scale(0.9) translateX(-20px);
        transition: 0.2s linear;
        position: relative;
        opacity: 0.7;
        font-weight: 700;
        letter-spacing: 2px;
        font-size: 1rem;
    }

    .label-up{
        transform: translateY(0) translateX(0);
        opacity: 1;
    }

    .leave-down{
        transform: translateY(0);
    }


    form div input{
        height: 2rem;
        width: 100%;
        border: none;
        border-bottom: 2px solid #000;
        position: relative;
        background-color: transparent;
        margin-bottom: 25px;
        outline: none;
        font-size: 1.3rem;
        padding: 2px;
        color: rgba(0,0,0,0.7)
    }

    form div button{
        margin: 20px 20%;
        font-size: 1rem;
        padding: 8px 0;
        border: none;
        transition: 0.2s ease-in;
    }

    form div button:hover{
        background-color: rgb(197, 197, 197);
    }

    form div small{
        font-size: 1rem;
        border: none;
        text-align: center;
        color: rgb(35, 168, 242);
    }



</style>

@endsection
@section('js')

    <script>
        function offline(abc){
            console.log(abc)
            var data =document.getElementById('chekedlesson')
            var chekedlessonOffline =document.getElementById('chekedlessonOffline')



if (abc =='offline'){
    data.checked=true
    chekedlessonOffline.checked=true
    document.getElementById('msg').innerHTML='Online'
    document.getElementById('onlineinvalid').innerHTML='{{$pricedata->onlinePersonal}}'
    document.getElementById('onlinegroupe').innerHTML='{{$pricedata->onlineGroup}}'
    chekedlessonOffline.style.display='none'
    data.style.display='table-header-group'
}else{
    data.checked=true
    chekedlessonOffline.checked=true
    document.getElementById('msg').innerHTML='Offline'
    document.getElementById('onlineinvalid').innerHTML='{{$pricedata->offlinePersonal}}'
    document.getElementById('onlinegroupe').innerHTML='{{$pricedata->offlineGroup}}'
    chekedlessonOffline.style.display='table-header-group'
    data.style.display='none'
}


        }


        const labels = document.querySelectorAll('.label')
        const formInputs = document.querySelectorAll('form div input')


        formInputs.forEach(formInput => {

            formInput.addEventListener('mouseover',() => {
                formInput.previousElementSibling.classList.add('label-up')
            })

            formInput.addEventListener('mouseleave',() => {
                if(formInput.value !== ''){
                    formInput.previousElementSibling.classList.add('label-up')
                    return
                }
                if(document.activeElement === formInput){
                    formInput.previousElementSibling.classList.add('label-up')
                    return
                }
                formInput.previousElementSibling.classList.remove('label-up')
            })

            formInput.addEventListener('blur',() => {
                if(formInput.value !== ''){
                    formInput.previousElementSibling.classList.add('label-up')
                    return
                }
                formInput.previousElementSibling.classList.remove('label-up')
            })

        })
    </script>
    @include('include.ajax')
@endsection
