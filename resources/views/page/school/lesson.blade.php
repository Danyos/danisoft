@extends('layouts.app')


@section('content')

    @include('include.slider')
    <div id="root">
        <div class="lesson_Lessons__583Aq"><h1></h1>
            <div class="MuiContainer-root MuiContainer-maxWidthLg"><h2></h2>
                @foreach($lesson as $lessons)
                    <div class="package_Package__2coC3">
                        <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-1">
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-3">
                                <div class="item_PackageItem__2f_eY">

                                    <img src="{{asset($lessons->avatar)}}" alt="{{$lessons->slug}}" style="width: 100px;">
                                </div>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-9">
                                <div class="content_RightItem__i2Cok"><h2>{{$lessons->title}}</h2>
                                        @if($lessons['description'])
                                    <h6><b>Նկարագրություն:</b>
                                        <span>{{$lessons['description']}}</span>
                                    </h6>
                                    @endif

                                    <p class="item_price__3COPF ">
                                        <span data-toggle="modal" data-target="#{{$lessons->slug}}" style="cursor: pointer; color: green;">Իմանալ ավելին   <i class="ti-zoom-in" ></i></span>

                                    </p>
                                    <button class="button_Button__3j--r button_primary__jND3m" onclick="location.href='{{route('lesson.register',$lessons->slug)}}'">
                                        Գրանցվել դասընթացներին
                                    </button>
                                    @if($lessons->more=='active')
                                    <p class="item_link__3xlcX">
                                        <button class="button_Button__3j--r"  onclick="location.href='{{route('special',$lessons->slug)}}'">Հատուկ
                                            դասընթացներ
                                        </button>
                                      </p>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="{{$lessons->slug}}" role="dialog" style="margin-top: 48px">
                        <!-- modal dialog -->
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- modal header -->
                                <div class="modal-header">
                                    <div class="mohe">
                                        <h4 class="modal-title">{{$lessons->title}}</h4>
                                    </div>
                                </div>
                                <!-- end modal header-->
                                <!-- modal body -->
                                <div class="modal-body">


                                        <fieldset>
                                            @if($lessons['language'])
                                                <h6 align="center">Այս փուլում կսովորենք</h6>
                                                <p>{{$lessons['language']}}</p>
                                                <p class="item_price__3COPF "><b><span>Առաջին դասը փորձնական անվճար է։</span></b></p>
                                            @endif
                                            @if($lessons->show=='both')
                                                <div class="lessontype">Online</div>
                                                <br>
                                                @if($lessons->onlinePersonal)
                                                    <p class="item_price__3COPF "><b><span style="color: black">
                                               <img src="{{asset('asset/lesson/person.svg')}}" alt="" class="MuiSvgIcon-root">
                                                                Գումարը </span>:
                                                        </b><span>{{$lessons->onlinePersonal}}</span><b> Ամիս/դրամ</b></p>
                                                @endif
                                                @if($lessons->onlineGroup)
                                                    <p class="item_price__3COPF "><b><span style="color: black"> <img
                                                                    src="{{asset('asset/lesson/group.svg')}}" alt=""
                                                                    class="MuiSvgIcon-root"> Գումարը  </span>:
                                                        </b><span>{{$lessons->onlineGroup}}</span><b> Ամիս/դրամ</b></p>
                                                @endif
                                                <div class="lessontype">Offline</div>
                                                <br>
                                                @if($lessons->show=='both')
                                                    @if($lessons->offlinePersonal)
                                                        <p class="item_price__3COPF "><b><span style="color: black"> <img
                                                                        src="{{asset('asset/lesson/person.svg')}}" alt=""
                                                                        class="MuiSvgIcon-root"> Գումարը </span>:
                                                            </b><span>{{$lessons->offlinePersonal}}</span><b> Ամիս/դրամ</b></p>
                                                    @endif
                                                    @if($lessons->offlineGroup)
                                                        <p class="item_price__3COPF "><b><span style="color: black"> <img
                                                                        src="{{asset('asset/lesson/group.svg')}}" alt=""
                                                                        class="MuiSvgIcon-root"> Գումարը  </span>:
                                                            </b><span>{{$lessons->offlineGroup}}</span><b> Ամիս/դրամ</b></p>
                                                    @endif
                                                @endif

                                            @elseif($lessons->show=='offline')
                                                <div class="lessontype">Offline</div>
                                                <br>
                                                <p class="item_price__3COPF "><b><span
                                                            style="color: black">Գումարը </span>:
                                                    </b><span>{{$lessons->offlinePersonal}}</span><b> Ամիս/դրամ</b></p>

                                            @elseif($lessons->show=='online')
                                                <div class="lessontype">Online</div>
                                                <br>
                                                @if($lessons->onlinePersonal)

                                                    <p class="item_price__3COPF "><b><span style="color: black"> <img
                                                                    src="{{asset('asset/lesson/person.svg')}}" alt=""
                                                                    class="MuiSvgIcon-root"> Գումարը </span>:
                                                        </b><span>{{$lessons->onlinePersonal}}</span><b> Ամիս/դրամ</b></p>
                                                @endif
                                                @if($lessons->onlineGroup)
                                                    <p class="item_price__3COPF "><b><span style="color: black"> <img
                                                                    src="{{asset('asset/lesson/group.svg')}}" alt=""
                                                                    class="MuiSvgIcon-root"> Գումարը  </span>:
                                                        </b><span>{{$lessons->onlineGroup}}</span><b> Ամիս/դրամ</b></p>
                                                @endif
                                            @endif
                                            <p class="item_duration__2vWJ1"><b>Դասաժամի տեվողությունը: </b><span>{{$lessons->time}} h</span></p>
                                            <p class="item_duration__2vWJ1"><b>Դասաժամի քանակը: </b><span>12 դաս</span>
                                            <p class="item_duration__2vWJ1"><b>Տեվողությունը: </b><span>{{$lessons->math}} ամիս</span>

                                        </fieldset>

                                </div>
                                <!-- end modal body -->
                                <!-- modal footer-->
                                <div class="modal-footer">
                                    <div class="mohe">
                                        <button  class="btn btn-default btn-md btn-sm" data-dismiss="modal"> Փակել</button>
                                    </div>
                                </div>
                                <!-- end modal footer -->
                            </div>
                            <!-- end modal content-->
                        </div>
                        <!-- end modal dialog-->
                    </div>
                @endforeach


            </div>

        </div>

    </div>

@endsection
@section('css')
    <style>
        .lessontype{
            display: inline;
            font-weight:bold;
            background: linear-gradient(15deg, black, transparent);
            color: #fff3cd;
            padding: 5px;
            text-align: center;
            position: relative;
            bottom: 7px;
            border-radius:40%;
        }
        .item_duration__2vWJ1 b {
            color: black;
        }

        .price-name {
            color: black;
        }
        .welcome-message .borderBtn {
            position: absolute;
            top: 300%;
            left: 45%;
            text-align: center;
            text-decoration: none;
            font-weight: 500;
            font-size: 1.05em;
            color: white;
            padding: 1.45em 3.35em;
            border-radius: 0.25em;
            background: rgba(195, 40, 40, 1);
        }

        .borderBtn:hover {
            color: white;
            background-color: #363B3F;
        }

        .container .btn-lg {
            background: rgba(195, 40, 40, 1);
            border: 0.05em solid rgba(195, 40, 40, .7);
            color: white;
        }

        .container .btn-lg:hover {
            background: white;
            border: 0.05em solid rgba(195, 40, 40, 1);
            color: rgba(195, 40, 40, 1);
        }

        .modal-body .form-control {
            border-radius: 0.35em;
            margin-right: 0.25em;
            padding: 0.15em;
        }

        .modal-body .form-group label {
            margin-left: 0.35em;
        }

        .modal-header {
            background: rgba(195, 40, 40, 1);
        }

        .modal-body .form-group {
            text-align: left;
        }

        .close {
            color: white !important;
            opacity: 0.8 !important;
        }

        .mohe {
            position: relative;
            display: block;
            color: white;
            float: left;
            padding: 0.75em;
        }



        .modal-footer {
            background: rgba(195, 40, 40, 1);
        }

        .modal-footer button {
            background: white;
            color: black;
            width: 9em;
            border-radius: 0.35em;
        }

        .input-group textarea {
            height: 6.35em !important;
            background-color: rgba(255, 255, 255, 0.9);
        }

        .form-control {
            height: 2em;
            padding: 0.75em 1.15em;
            font-size: 1.15em;
            border: 0.05em solid rgba(195, 40, 40, 1) !important;
            background-color: rgba(255, 255, 255, 0.9) !important;
            color: black;
        }

        .input-group .btn-primary {
            color: rgba(255, 255, 255, 1);
            background-color: rgba(195, 40, 40, 0.9) !important;
            width: 50% !important;
            float: right !important;
            margin-right: 0 !important;
        }

        .input-group .btn-primary:hover {
            background-color: rgba(255, 255, 255, 1) !important;
            color: #363B3F;
            border: 0.15em solid rgba(195, 40, 40, 1);
            border-radius: 0.35em;
            font-weight: 700;
        }
    </style>


@endsection
