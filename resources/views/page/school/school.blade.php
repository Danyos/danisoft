@extends('layouts.app')
@section('content')
<section class="section bg-home" id="home">
    <!-- <div class="bg-overlay"></div> -->
    <div class="home-center">
        <div class="home-desc-center">
            <div class="container">
                <div class="row vertical-content">
                    <div class="col-lg-7" >
                        <div>
                            <h1 class="text-white home-title mb-0">ԾՐԱԳՐԱՎՈՐՈՂԻ ՄԱՍՆԱԳԻՏՈՒԹՅՈՒՆ</h1>
                            <p class="text-muted home-subtitle mt-4 mb-0">
                                Դառնալ ծրագրավորող զրոյից
                                <br>
                                Կառուցեք հաջող կարիերա ՏՏ ոլորտում
                                <br>
                                Աշխատանքի տեղավորում Հայաստանում և արտասահմանում </p>

                        </div>
                    </div>
                    <div class="col-lg-5 mt-3">
                        <div class="home-registration-form mx-auto bg-white w-75 p-4">
                            <h5 class="form-title mb-4 text-center font-weight-bold">Անվճար 1 դասի
                                հնարավորություն</h5>
                            <div class="alert alert-success d-none" id="msg_div">
                                <span id="res_message"></span>
                            </div>
                            <form id="contact_us" method="post" action="javascript:void(0)"  class="registration-form">
                                @csrf

                                <label class="text-muted" for="InputName1">Անուն</label>
                                <input type="text" name="name" id="InputName1"  class="form-control mb-2 registration-input-box">
                                <span class="text-danger">{{ $errors->first('name') }}</span>


                                <label class="text-muted" for="Inputage">Տարիք</label>
                                <input type="number" name="age" id="Inputage" class="form-control mb-2 registration-input-box">
                                <span class="text-danger">{{ $errors->first('age') }}</span>


                                <label class="text-muted" for="InputTel">Հեռախոսահամար</label>
                                <input type="tel" name="tel" id="InputTel" class="form-control mb-2 registration-input-box">
                                <span class="text-danger">{{ $errors->first('tel') }}</span>



                                <label class="text-muted" for="Inputemail">Փոստ</label>
                                <input type="email"  name="email" id="Inputemail" class="form-control mb-2 registration-input-box">
                                <span class="text-danger">{{ $errors->first('email') }}</span>

                                <button class="btn btn-custom w-100 mt-3 text-uppercase" type="submit" id="send_form">Հաստատել</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END HOME -->

<!-- Start Services -->
<section class="section" id="services">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <img src="{{asset('asset/images/school/WHY-BECOME-PROGRAMMER.png')}}" class=" text-muted" alt="ԻՆՉՈՒ՞ ԴԱՌՆԱԼ ԾՐԱԳՐԱՎՈՐՈՂ" style="width: 70px">


                <h3 class="title">ԻՆՉՈՒ՞<span class="font-weight-bold"> ԴԱՌՆԱԼ </span></h3>
                <p class="text-muted mt-3 title-subtitle mx-auto"><span class="font-weight-bold">ԾՐԱԳՐԱՎՈՐՈՂ</span>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">

                        <img src="{{asset('asset/img/icons/why/money.svg')}}" alt="Բարձր աշխատավարձ" class="text-custom" style="width:70px;">

                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Բարձր աշխատավարձ</h5>
                        <p class="services-subtitle text-muted">
                            Կրտսեր ծրագրավորողները ամսական վաստակում են ավելի քան 200-300 հազար ՀՀ դրամ, իսկ լավագույնները երկուսից երեք անգամ ավելի են վաստակում, մինչդեռ հավաքագրողները վազում են նրանց ետևից:

                        </p>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/web.svg')}}" alt="Յուրահատուկ աշխատանքաին պայմաներ" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Յուրահատուկ աշխատանքաին պայմաներ</h5>
                        <p class="services-subtitle text-muted"> ՏՏ ընկերություները ներգրավում են մեծ թվով
                            մասնագետների։
                            Այս ոլորտում թափուր աշխատատեղերը շատ են։Հետեվաբար, ծրագրավորողները, բացի բարձր
                            աշխատավարձերից, ունեն նաև բարենպաստ աշխատանքային պայմաներ։</p>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/bars.svg')}}" alt="Աշխատուժի աճող շուկա" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Աշխատուժի աճող շուկա</h5>
                        <p class="services-subtitle text-muted"> Տեղեկատվական տեխնոլոգիաները (ՏՏ)  ոլորտը Հայաստանի Հանրապետությունում ամենահաջողակ և արագ զարգացող ճյուղերից է, </p>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/location.svg')}}" alt="Աշխատանք աշխարհի ցանկացած վայրում" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Աշխատանք աշխարհի ցանկացած վայրում</h5>
                        <p class="services-subtitle text-muted"> Դու կարող եք աշխատել արտասահմանյան նախագծերի վրա՝
                            Հայաստանյան ընկերությունների արտաքին ռեսուրսների ներգրավմամբ։ Կարող եք աշխատել աշխարհի
                            ցանկացած երկրում։ Ծրագրավորողի մասնագիտությունը տալիս է շատ լայն
                            հնարավորություններ։.</p>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/briefcase.svg')}}" alt="Երաշխավորված զբաղվածություն" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Մասնագիտական առավելություններ</h5>
                        <p class="services-subtitle text-muted"> Նրանք ստանում են բազմաթիվ առավելություններ` անվճար սննդից և ընդլայնված բժշկական ապահովագրությունից մինչև Apple-ի նոր տեխնոլոգիաներ և երեխաների համար կորպորատիվ դայակներ: </p>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/lifesaver.svg')}}" alt="Աշխարհը փոխելու իրական հնարավորություն" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Աշխարհը փոխելու իրական հնարավորություն</h5>
                        <p class="services-subtitle text-muted"> Անկախ նրանից, թէ ինչ եք ուզում ձեր
                            մասնագիտությունից՝ մեծ գումար աշխատել թէ փոխել աշխարհը, ծրագրավորումով դուք երկու
                            հնարավորությունն էլ ունեք։ Ո՞վ գիտե, միգուցե հենց դուք կստեղծեք մի նախագիծ, որը կփոխի
                            մեր գաղափարները խանոթ բաների վերաբերյալ։.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/like.svg')}}" class="text-muted" alt="Պահանջված մասնագիտություն" style="width: 70px">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Պահանջված մասնագիտություն</h5>
                        <p class="services-subtitle text-muted"> Համակարգիչները այսօրվա մեր կյանքի բոլոր ոլորտներում
                            են։
                            Դժվար է պատկերացնել ավելի ժամանակակից մասնագիտություն քան ծրագրավորումը։</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/programmer.svg')}}" alt="" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Մեծ հեռանկարային մասնագիտություն</h5>
                        <p class="services-subtitle text-muted"> Տեղետվական տեխնոլոգիաների ոլորտը ամենադինամիկ զարգացող
                            ոլորտներից մեկն է ամբողջ աշխարհում։
                            Այստեղ տեղյակ կլինեք նորությունների մասին։</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box">
                    <div class="services-icon">
                        <img src="{{asset('asset/img/icons/why/bl2-icon3.png')}}" alt="Ինչ կարող է տալ քեզ" class=" text-custom"
                             style="width:70px;">
                    </div>
                    <div class="mt-3">
                        <h5 class="services-title font-weight-bold mb-3">Ինչ կարող է տալ քեզ</h5>
                        <p class="services-subtitle text-muted">Ծրագրավորումը նպաստում է մարդուն մտքի,
                            տրամաբանության և արագ կողմորոշվելու,
                            ամենաօպտիմալ եզերքը գտնելու տվյալ խնդրի վերաբերյալ։ </p>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
</section>
<!-- End Services -->


<!-- Start Features -->
<section class="section bg-light" id="features">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">


                <img src="{{asset('asset/images/school/WHAT-SHOULD-YOU-KNOW.jpg')}}" class="text-muted" alt="ԻՆ՞Չ Է ՊԵՏՔ ԻՄԱՆԱԼ" style="width: 70px">
                <h3 class="title">ԻՆ՞Չ Է ԱՆՀՐԱԺԵՇՏ
                    <span class="font-weight-bold">
    <br><span class="font-weight-bold">ԾՐԱԳՐԱՎՈՐՈՂ</span>
</span></h3>
                <p class="text-muted mt-3 title-subtitle mx-auto">ԼԻՆԵԼՈՒ ՀԱՄԱՐ</p>
            </div>
        </div>
        <div class="row mt-5 vertical-content">
            <div class="col-lg-6 mt-2">
                <div>
                    <img src="{{asset('asset/images/school/TO-BE-A-PROGRAMMER.png')}}" alt=""
                         class="img-fluid mx-auto d-block">
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1 mt-2">
                <div class="features-desc">


                    <ul class="whatdo">
                        <li>Ցանկություն</li>
                        <li>Հստակ նպատակ</li>
                        <li>Վերլուծական մտածողություն.</li>
                        <li>Տրամաբանությանը առկայություն</li>
                        <li>Երկնքից աստղ բռնելու սովորություն</li>
                        <li>Ուշադրություն</li>
                        <li>Սեփական ուժերի հանդեպ վստահություն</li>
                        <li>Ստեղծարար մտածելակերպ</li>
                        <li>Օտար լեզուների իմացությունը ցանկալի է</li>


                    </ul>
                    <p class="text-muted mt-3" style="font-weight:900; font-size:18px; color:red !important;">Եթե դուք ցանկանում եք ինքնուրույն սովորել, դուք կարող եք ունենալ
                        ավելի շատ հարցեր, քան պատասխաններ</p>

                    <!--   <a href="#" class="btn btn-custom btn-round mt-3">Read more<i class="mdi mdi-chevron-right"></i></a> -->
                </div>
            </div>
        </div>

</section>

<section class="section bg-light" id="team">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <img src="{{asset('asset/images/school/team.png')}}" class="text-muted" style="width: 80px" alt="Թիմը">

                <h3 class="title">Մեր <span class="font-weight-bold">Թիմը</span></h3>
                <!-- <p class="text-muted mt-3 title-subtitle mx-auto">It is a long established fact that a reader will be of a page when established fact looking at its layout.</p> -->
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
                <div class="text-center bg-white team-box mt-3 pl-4 pr-4 pt-5 pb-5">
                    <div class="">
                        <img src="{{asset('asset/images/team/daniel.jpg')}}" alt="Դանիել Համբարձումյան"
                             class="img-fluid rounded-circle mx-auto d-block" width="170px"
                             style="transform: rotate(-11deg);">
                    </div>
                    <div class="team-name">
                        <p class="font-weight-bold mb-0 mt-4">Դանիել Համբարձումյան</p>
                        <p class="text-muted mt-4">FullStack Ծրագրավորող</p>
                    </div>
                    <div class="">
                        <ul class="list-inline team-social mt-4 mb-0">
                            <li class="list-inline-item"><a href="https://www.facebook.com/profile.php?id=100011853108441"><i class="ti-facebook"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="text-center bg-white team-box mt-3 active pl-4 pr-4 pt-5 pb-5">
                    <div class="">
                        <img src="{{asset('asset/images/team/vruyr.jpg')}}" alt="Վրույր Սարիբեկյան"
                             class="img-fluid rounded-circle mx-auto d-block" width="170px">
                    </div>
                    <div class="team-name">
                        <p class="font-weight-bold mb-0 mt-4">Վրույր Սարիբեկյան</p>
                        <p class="text-muted mt-4">Frontend ծրագրավորող</p>
                    </div>
                    <div class="">
                        <ul class="list-inline team-social mt-4 mb-0">
                            <li class="list-inline-item"><a href="https://www.facebook.com/vruyr.saribekyan"><i class="ti-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="text-center bg-white team-box mt-3 pl-4 pr-4 pt-5 pb-5">
                    <div class="">
                        <img src="{{asset('asset/images/team/117845098_3423505117716848_1474157954365497623_o.jpg')}}" width="170px" alt="Հայկ Մալխասյան" class="img-fluid rounded-circle mx-auto d-block">
                    </div>
                    <div class="team-name">
                        <p class="font-weight-bold mb-0 mt-4">Հայկ Մալխասյան</p>
                        <p class="text-muted mt-4">Frontend ծրագրավորող</p>
                    </div>
                    <div class="">
                        <ul class="list-inline team-social mt-4 mb-0">
                            <li class="list-inline-item"><a href="https://www.facebook.com/hayko.malxasyan"><i class="ti-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Team -->

<!-- Start Faq -->
<section class="section" id="faq">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <img src="{{asset('asset/images/danisoft_logo.png')}}" class="text-muted" alt="Danisoft" style="width: 140px">
                <h3 class="title">ԻՆՉՈՒ ԸՆՏՐԵԼ <span class="font-weight-bold">Danisoft</span></h3>
                <p class="text-muted mt-3 title-subtitle mx-auto">Հիմա պարզենք ինչու՞ հենց Danisoft</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6">
                <div class="pl-4 pr-4">
                    <div class="mt-4">
                        <div class="">
                            <h5 class="mb-0 faq-que">Ժամանակակից պահանջներին համապատասխան ծրագիր</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Դասընթացի ծրագիրը կառուցված է շուկայի պահանջներին համապատասխան և պարբերաբար
                                թարմացվում է: Մենք սովորեցնում ենք Ձեզ այն, ինչ հարկավոր է լավ մասնագետ դառնալու համար առանց ավելորդ անգամ Ձեզ ծանրաբեռնելու:</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Եզակի ուսումնամեթոդական նյութեր</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Դասընթացի ծրագիրը չունի անալոգներ: Հեղինակային մեթոդները,
                                էլեկտրոնային դասերը, ուսումնական պլանները, ուսումնական գործընթացում իրական
                                նախագծերը: Այս ամենը ապահովում է մեր շրջանավարտների գիտելիքների բարձր մակարդակը.</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Ժամանակակից գիտելիքներ</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Փորձառու մասնագետները մեր դիմորդներին կփոխանցեն իրենց ոլորտի
                                լավագույն փորձը՝ տալով արդի ու կիրառական գիտելիքներ:.</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Վկայականի հանձնում</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Շրջանավարտները ստանում են նմուշի վկայական դասընթացի հաջող ավարտից
                                հետո</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="pl-4 pr-4">
                    <div class="mt-4">
                        <div class="">
                            <h5 class="mb-0 faq-que">100% գործնական դասընթաց</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Ուսուցման գործընթացը հիմնված է գործնական խնդիրների և ուսուցչի հետ
                                շփման վրա: Մենք միանման դասախոսություններ չունենք: Եվ գիտելիքն անմիջապես
                                գործնականում ամրագրված է:</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Հարմար դասի ժամանակացույց</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Կարող եք ընտրել դասերի հարմար ժամանակ և օրեր (առավոտ, կեսօր,
                                երեկո, աշխատանքային օրեր կամ հանգստյան օրեր): Danisoft-ում ուսումը կարող է
                                զուգորդվել ուսման հետ մեկ այլ համալսարանում կամ աշխատանքի մեջ:</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Այլընտրանքի բացակայություն</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Մեր շրջանավարտներն ուսումնական կենտրոնն ավարտելուց հետո կարող են
                                դիմել իրենց դասախոսներին՝ մասնագիտական խորհրդատվություն ստանալու համար:</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="">
                            <h5 class="mb-0 faq-que">Աշխատանքի տեղավորման աջակցություն</h5>
                        </div>
                        <div class="faq-ans">
                            <p class="text-muted">Ուսումնական կենտրոնի շրջանավարտների համար իրական թափուր
                                աշխատատեղերի առկայություն:</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section bg-light" id="pricing">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">

                <img src="{{asset('asset/images/school/school_price.svg')}}" class=" text-muted" alt="ԸՆՏՐԻՐ ՔԵԶ ՀԱՐՄԱՐ" style="width: 80px">
                <h3 class="title">ԸՆՏՐԻՐ ՔԵԶ
                    <span class="font-weight-bold">ՀԱՐՄԱՐ</span></h3>
                <p class="text-muted mt-3 title-subtitle mx-auto"><span class="font-weight-bold">ՏԱՐԲԵՐԱԿ</span></p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4" style="border:1px solid #EB1C26;box-shadow: -9px -14px 5px 8px #EB1C26;border-radius: 46px;">
                <div class="bg-white price-box text-center mt-3">
                    <div class="plan-price font-weight-bold">
                        <h2 class="mb-0 font-weight-bold">Անհատական</h2>
                        <p class="mb-0">Offline</p>
                    </div>
                    <div class="plan-features text-muted mt-5 mb-5">

                        <span>Տեվողություն</span>
                        <p class="text-custom">4-6 ամիս</p>
                        <span>Ամիսը</span>
                        <p class="text-custom">12 դաս</p>
                        <span>Դասաժամը</span>
                        <p class="text-custom"><br>90 րոպե</p>
                        <span>Առավելագույն քանակը</span>
                        <p class="text-custom"><br>Միայն դուք</p>
                        <span>Վարձավճարը</span>
                        <p class="text-custom">59 հազար դրամ/ Ամիս</p>
                    </div>
                    <div>
                        <a  class="btn btn-custom btn-round scrole" style="background:#EB1C26 !important;">Գրանցվել</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" style="border:1px solid #26295E;box-shadow:-1px -20px 12px 3px #26295E;border-radius: 46px;">
                <div class="bg-white price-box active text-center mt-3" >
                    <div class="plan-price font-weight-bold">
                        <h2 class="mb-0 font-weight-bold">Խմբակային</h2>
                        <p class="mb-0">Offline</p>
                    </div>
                    <div class="plan-features text-muted mt-5 mb-5">
                        <span>Տեվողություն</span>
                        <p class="text-custom">4-6 ամիս</p>
                        <span>Ամիսը</span>
                        <p class="text-custom">12 դաս</p>
                        <span>Դասաժամը</span>
                        <p class="text-custom"><br>90 րոպե</p>
                        <span>Առավելագույն քանակը</span>
                        <p class="text-custom"><br>4 հոգի</p>
                        <span>Վարձավճարը</span>
                        <p class="text-custom">29 հազար դրամ/ Ամիս</p>

                    </div>
                    <div>
                        <a  class="btn btn-custom btn-round scrole" style="background:#26295E !important;">Գրանցվել</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" style="border:1px solid #F9A72A;box-shadow: 13px -9px 5px 8px #F9A72A;border-radius: 46px;">
                <div class="bg-white price-box text-center mt-3">
                    <div class="plan-price font-weight-bold">
                        <h1 class="mb-0 font-weight-bold"> Անհատական</h1>
                        <p class=" mb-0">Online</p>
                    </div>
                    <div class="plan-features text-muted mt-5 mb-5">
                        <span>Տեվողություն</span>
                        <p class="text-custom">4-6 ամիս</p>
                        <span>Ամիսը</span>
                        <p class="text-custom">12 դաս</p>
                        <span>Դասաժամը</span>
                        <p class="text-custom"><br>90 րոպե</p>
                        <span>Առավելագույն քանակը</span>
                        <p class="text-custom"><br>Միայն դուք</p>
                        <span>Վարձավճարը</span>
                        <p class="text-custom">49 հազար դրամ/ Ամիս</p>
                    </div>
                    <div>
                        <a  class="btn btn-custom btn-round scrole" style="background:    #F9A72A !important;">Գրանցվել </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pricing -->

@endsection
@section('css')
    <style>
        .container {
            width: 90%;
        }
    </style>
@endsection
@section('js')
    @include('include.ajax')

    <script>
        $('.scrole').click(function (){
            $(window).scrollTop($('#topbar').offset().top)
        })

    </script>
@endsection
