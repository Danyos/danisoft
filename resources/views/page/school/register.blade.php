<!DOCTYPE html>
<html>
<head>
    <title>Գրանցվել {{$data}} դասընթացին</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('asset/css/register.css')}}" type="text/css">
    <link rel="icon" href="{{asset('asset/images/danisoft.png')}}">
</head>
<body>
<div class="bg-img">
    <div class="content">
        <header>
            <a href="{{route('index')}}">
                <img src="{{asset('asset/images/danisoft.png')}}" alt="{{$data}} դասընթացին" height="100px">
            </a>
            <br>
            <span>Գրանցվել դասընթացներին</span></header>
        <div class="alert alert-success d-none" id="msg_div">
            <span id="res_message"></span>
        </div>
        <form id="contact_us" method="post" action="javascript:void(0)">
            @csrf
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger">{{ $errors->first('name') }}</span>
                <input type="text" placeholder="Անուն" name="name" id="formGroupExampleInput">
                <br>
                <br>
            </div>
            <br>
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger">{{ $errors->first('age') }}</span>
                <input type="number" required placeholder="Տարիք" name="age">

            </div>
            <br>

            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger">{{ $errors->first('tel') }}</span>
                <input type="tel" name="tel" placeholder="Հեռախոսահամր">

            </div>
            <br>
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger">{{ $errors->first('email') }}</span>
                <input type="text" placeholder="Փոստ" name="email" id="email">


                <input type="hidden" value="{{$data}}" name="slug">
            </div>
            <br>

            <div class="pass">
                <br>
            </div>
            <div class="field">
                <input type="submit" value="Հաստատել" id="send_form">
            </div>
        </form>
        <br>
        <div class="signup"> Սովորւր մեկ անգամ օգտագործիր ամբողջ մի կյանք.
        </div>
    </div>
</div>


<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script src="{{asset('asset/js/popper.min.js')}}"></script>


<script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
@include('include.ajax')
</html>
