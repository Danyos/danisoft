<div class="works">

    <div class="advanced">
        <h2>Առաջարկվող ծառայություների փաթեթ</h2>
        <div class="row upset link-hover shape-num justify-content-center">
            @foreach($service as $k=>$servicesmain)
            <div class="col-lg-4 col-sm-6 mt30 shape-loc wow fadeInUp" data-wow-delay="0.2s"
                 style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <div class="s-block" data-tilt="" data-tilt-max="5" data-tilt-speed="1000"
                     style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1);">

                    <div class="s-card-icon"><img src="{!! $servicesmain->url !!}" alt="{{$servicesmain->slug}}"  class="img-fluid"></div>
                    <br>
                    <h4>{{$servicesmain->title}}</h4>
                    <p>{!! $servicesmain->offer !!}</p>
                    <a href="{{route('service',$servicesmain->slug)}}">Իմանալ ավելին </a>
                </div>
            </div>
            @endforeach

        </div>
    </div>


</div>
