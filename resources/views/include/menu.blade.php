<div class="wrapper" onclick="closeHandler(document.querySelector('.menu-btn'))" data-toggle=".menu">

</div>

<div class="menu" style="z-index: 99999">
    <button class="sClose" onclick="closeHandler(document.querySelector('.menu-btn'))" data-toggle=".menu">
        <i class="ti-shift-right-alt"></i>
    </button>

    <div class="menu-content">
        <ul class="navbar-nav menu-nav text-center">
            <li class="nav-item d-block d-md-none">
                <br>
            </li>

            <li class="nav-item {{ request()->is('our-Work') || request()->is('our-Work') ? 'redactive' : '' }}">
                <a class="nav-link" href="{{route('our.work')}}" title="Home">Մեր կատարած աշխատանքները</a>
            </li>


            <div style="border-bottom: 1px solid red;width:100%;"></div>
            <li class="nav-item {{ request()->is('lesson') || request()->is('lesson') ? 'redactive' : '' }}">
                <a class="nav-link" href="{{route('lesson')}}" title="Courses">Դասընթացներ</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('lesson.register')}}" title="Order video tutorials">Գրանցվել դասընթացին</a>
            </li>
            <li class="nav-item  {{ request()->is('school') || request()->is('school') ? 'redactive' : '' }}">
                <a class="nav-link" href="{{route('school.lesson')}}" title="Order video tutorials">Ինչու սովորել ծրագրավորում</a>
            </li>
        </ul>

    </div>
</div>

<nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky">
    <div class="container">
        <a class="navbar-brand" href="{{route('index')}}">
            <img src="{{asset('asset/images/danisoft.png')}}" class="logo"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="ti-menu"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item  {{ request()->is('/') || request()->is('/') ? 'active' : '' }} homeclass">
                    <a class="nav-link active" href="{{route('index')}}">
                        Գլխավոր</a></li>

                <li class="nav-item   {{ request()->is('/aboutUs') || request()->is('aboutUs') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('about')}}">Մեր Մասին</a>
                </li>  
                <li class="nav-item d-md-block d-lg-none {{ request()->is('/*-and-pricing') || request()->is('*-and-pricing') ? 'active' : '' }}">
                    <a class="nav-link " href="https://danisoft.am/web-and-pricing">Ծառայություններ</a>
                </li>

<li class="dropdown d-md-none d-sm-none d-xs-none d-lg-block services" style="margin-top:8px;" id="servicemain">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"
                       oncancel="location.href='{{route('service')}}">Ծառայություններ
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="background-color: black;width: 400px;">
                        @foreach($service as $services)
                            @isset($service_id)
                                @if($services->slug== $service_id)
                                    <li><a href="{{route('service',$services->slug)}}"
                                           id="{{ request()->is('/*') || request()->is('*') ? 'coloractive' : '' }}" >{{$services->title}}</a>
                                        <script>
                                            document.getElementById("servicemain").classList.add('active');
                                        </script>
                                    </li>
                                @else
                                    <li><a href="{{route('service',$services->slug)}}">{{$services->title}}</a>
                                    </li>
                                @endif
                            @else
                                <li><a href="{{route('service',$services->slug)}}"
                                       id="{{ request()->is('/services*') || request()->is('services*') ? 'coloractive' : '' }}">{{$services->title}}</a>
                                </li>

                            @endisset
                        @endforeach
                    </ul>
                </li>

              
                <li class="dropdown d-md-block d-lg-none {{ request()->is('/school') || request()->is('school') ? 'active' : '' }}" style="margin-top:8px;">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="{{route('lesson')}}">Դասընթացներ
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="background-color: black;">
                        <li class="{{ request()->is('/school') || request()->is('school') ? 'active' : '' }}">
                            <a href="{{route('school.lesson')}}">Ինչու սովորել</a>
                        </li>

                        <li><a href="{{route('lesson')}}">Դասընթացներ</a></li>
                        <li><a href="{{route('lesson.register')}}">Գրանցվել</a></li>
                    </ul>
                </li>

                <li class="nav-item {{ request()->is('our-Work') || request()->is('our-Work') ? 'active' : '' }} ">
                    <a class="nav-link d-md-block d-lg-none" href="{{route('our.work')}}">Աշխատանքներ</a>
                </li>
                <li class="nav-item {{ request()->is('blog/news') || request()->is('blog/news') ? 'active' : '' }} ">
                    <a class="nav-link d-md-block d-lg-none" href="{{route('blognews')}}">Blog</a>
                </li>
                <li class="nav-item {{ request()->is('/contact') || request()->is('contact') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('contact')}}">Կապ</a>
                </li>


            </ul>
            <div>
                <ul class="text-right list-unstyled list-inline mb-0 nav-social">

                    <li class="list-inline-item"><a href="https://www.facebook.com/danisoft.am" class="facebook"><i
                                class="ti-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="https://www.instagram.com/danisoft.am" class="facebook"><i
                                class="ti-instagram"></i></a></li>
                    <li class="list-inline-item"><a href="{{route('blognews')}}">
                            <img src="{{asset('asset/images/blog.png')}}" class=" text-muted" alt="blog" style="width: 40px"></a></li>

                    <li class="list-inline-item">
                        <button class="menu-btn" onclick="openHandler(this)" data-toggle=".menu">
                            <i class="ti-align-right"></i>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
