<div class="works">
    <div class="worksContent1">
        <h1>Պարզ գործընթաց՝ մինչև գործարկել</h1>
        <div class="w-c-d">
            <div class="row">
                @foreach($blogItems as $blog )
                 <div class="cardsItem1 col-xs-10 col-sm-10 col-md-4 " style="padding-top: 20px" onclick="location.href='{{route('blogid',$blog->slug)}}'">
                    <div>
                        <img
                            src="{{asset($blog->avatar)}}" alt="{{$blog->slug}}" style="height: 300px;object-fit: cover">
                        <button class="btn">
                            <i class="ti-link"></i>
                        </button>
                        
                
                         <ul class="newname">
                                <li >
                                    <span class="step">{{$blog->name}}</span>
                                </li>
                             
                            </ul>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
</div>
