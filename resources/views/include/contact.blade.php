<div style="width:100%;background-color: white !important;">
<div class="worksContent1" style="background-color: white !important;">
    <h1>Կապ մեզ հետ</h1>
</div>
@if(session()->has('succsess'))
    <span class="help-block">
                        <p class="alert alert-success" align="center">
                <strong>Հաստատված!</strong> Մեր մասնագետները կկապվեն ձեզ հետ.
                        </p>
                       </span>
@endif
<form id="contact-form" class="form cf" action="{{route('send')}}" method="post" >
    @csrf


    <div class="col cf">

        <div class="form-row cf">
            <label for="input-name">Անուն: *</label>
            <input type="text" id="input-name" name="name" value="{{old('name')}}" >
            @if ($errors->has('name'))
                <br>
                <span class="help-block">
                        <p class="alert alert-danger"> <strong>Внимание!</strong> Փորձեք ճիշտ լրացնել դաշտերը. </p>
                       </span>
            @endif
        </div>


        <div class="form-row cf">
            <label for="input-email">Փոստ: *</label>
            <input type="text"  id="input-email"  name="email" value="{{old('email')}}" required>
            @if ($errors->has('email'))
                <br>
                <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Սխալ!</strong> Փորձեք ճիշտ լրացնել դաշտերը.
                        </p>
                       </span>
            @endif

        </div>
    </div>

    <div class="col cf">

        <div class="form-row cf">
            <label for="input-phone">Հեռախոսահամար: *</label>
            <input type="text" id="input-phone" name="tel" value="{{old('tel')}}" required>
            @if ($errors->has('tel'))
                <br>
                <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Սխալ!</strong>Փորձեք ճիշտ լրացնել դաշտերը.
                        </p>
                       </span>
            @endif

        </div>


    </div>

    <div class="form-row col-100 cf">
        <label for="textarea-message">Բովանդակություն: *</label>
        <textarea id="textarea-message"
                  style="margin-top: 0px; margin-bottom: 0px; height: 120px;" name="message" >{{old('message')}}</textarea>
        @if ($errors->has('message'))
            <br>
            <span class="help-block">
                        <p class="alert alert-danger">
                <strong>Սխալ!</strong> Փորձեք ճիշտ լրացնել դաշտերը.
                        </p>
                       </span>
        @endif

    </div>

    <div class="form-row col-100 cf">
        <input type="submit" name="submit"  value="Ուղարկել" class="btn send-btn mySendBtn">
    </div>
</form>
</div>
