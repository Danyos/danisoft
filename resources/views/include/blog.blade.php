<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>

    if ($("#contact_us").length > 0) {

        $("#contact_us").validate({


            rules: {

            name: {

                required: true,
                    digits: false,
                    maxlength: 50
                },


            email: {

                required: true,

                    maxlength: 50,

                    email: true,

                },

                message: {

                required: true,
                    minlength: 5,
                },

        },

            messages: {


            name: {

                required: "Խնդրում ենք լրացնել Անվանման դաշտը",

                    maxlength: "Շատ երկար է դաշտը"

                },



            email: {

                required: "Խնդրում ենք լրացնել փոստը դաշտը",

                    email: "Գրեք ձեր փոստը",

                },


                message: {

                required: "Խնդրում ենք լրացնել տարիքի դաշտը",
                    minlength: "Կարձ է",

                },



        },

            submitHandler: function (form) {

            $.ajaxSetup({

                    headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                $('#send_form').html('Բեռնում..');

                $.ajax({

                    url: '{{route('blog.created')}}',

                    type: "POST",

                    data: $('#contact_us').serialize(),

                    success: function (response) {

                        if (response=='error'){
                            $('#res_message').html('Ինչ-որ բան այն չէ: Խնդրում եմ փորձեք կրկին');
                        }
                $('#send_form').html('Հաստատված');
                        if (response!='error') {
                            $('#res_message').show();
                            $('#msg').append(response);

                        }
                $('#msg_div').removeClass('d-none');


                document.getElementById("contact_us").reset();

                setTimeout(function () {

                    $('#res_message').hide();

                    $('#msg_div').hide();

                }, 4000);

            }

                });

            }

        })

    }

</script>
