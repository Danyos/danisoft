<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>

    if ($("#contact_us").length > 0) {

        $("#contact_us").validate({


            rules: {

            name: {

                required: true,
                    digits: false,
                    maxlength: 50
                },
            tel: {
                required: true,

                    digits: true,

                    minlength: 9,

                    maxlength: 14,

                },

            email: {

                required: true,

                    maxlength: 50,

                    email: true,

                },
            age: {

                required: true,
                    digits: true,
                    minlength: 2,
                    maxlength: 2,


                },

        },

            messages: {


            name: {

                required: "Խնդրում ենք լրացնել Անվանման դաշտը",

                    maxlength: "Շատ երկար է դաշտը"

                },

            tel: {

                required: "Խնդրում ենք լրացնել հեռախոսահամարի դաշտը",

                    digits: "Միայն թիվ",

                    minlength: "Ձեր հեռախոսահամրը չի համապատսխանում",

                    maxlength: "Գրեք ձեր ճիշտ հեռախոսահամրը",

                },

            email: {

                required: "Խնդրում ենք լրացնել փոստը դաշտը",

                    email: "Գրեք ձեր փոստը",

                },
            age: {

                required: "Խնդրում ենք լրացնել տարիքի դաշտը",
                    digits: "Միայն թիվ",

                    minlength: "Ձեր տարիքը չի համապատսխանում",

                    maxlength: "Գրեք ձեր ճիշտ տարիքը"
                },



        },

            submitHandler: function (form) {

            $.ajaxSetup({

                    headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                $('#send_form').html('Բեռնում..');

                $.ajax({

                    url: '{{route('lessonRegister.store')}}',

                    type: "POST",

                    data: $('#contact_us').serialize(),

                    success: function (response) {

                $('#send_form').html('Հաստատված');

                $('#res_message').show();

                $('#res_message').html(response.msg);

                $('#msg_div').removeClass('d-none');


                document.getElementById("contact_us").reset();

                setTimeout(function () {

                    $('#res_message').hide();

                    $('#msg_div').hide();
                    $('#send_form').html('Հաստատել');
                }, 4000);

            }

                });

            }

        })

    }

</script>
