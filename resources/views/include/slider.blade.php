<div class="custom-slider">
    <div class="custom-slide custom-slide1 active"
         style="background:url('{{asset('asset/images/slider/Slider2.jpg')}}')">
        <div class="custom-caption">
            <h2 class="text-center font-weight-light">
                        <span style="margin-bottom: 80px;">Այն, ինչ կարող ես պատկերացնել, դա իրական է</span>
            </h2>

        </div>
    </div>
    <div class="custom-slide custom-slide2" style="background:url('{{asset('asset/images/slider/danisoft_slider.jpg')}}')">
        <div class="custom-caption">
            <h2 class="text-center font-weight-light">
                        <span style="margin-bottom: 80px;">Խենթ գաղափարներ: Այսպես ենք կառուցում վաղվա աշխարհը այսօր</span>
            </h2>

        </div>
    </div>
    <div class="custom-slide custom-slide3" style="background:url('{{asset('asset/images/slider/office.jpg')}}')">
        <div class="custom-caption">
            <h2 class="text-center font-weight-light">
                        <span style="margin-bottom: 80px;">Աշխարհում չիրականացող բան չկա, միայն պետք է մտածել.</span>
            </h2>

        </div>
    </div>
</div>
