@extends('layouts.app')
@section('content')
 <!--<div class="bodys" id="loading"><h2 data-text="Danisoft..." class="loader">Danisoft...</h2></div>-->
    <div>
        <div class="main-section">
            @include('include.slider')
  
            <div class="aboutUs">
                <div class="contentAb">
                    <h1>Մեր Մասին</h1>
                    <div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="whoWeAre">
                                    <div class="whoWeAreIn">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6 wwai-item">
                                                <div class="wwai-left">
                                                    <h3 style="color:black;">Մենք տաղանդ ունենք տեխնոլոգիայի ապագան
                                                        փոխակերպելու:</h3>
                                                    <div class="left-text">
                                                        Մենք զբաղվում ենք վեբ կայքերի մշակմամբ նրանց՝ հետագա աջակցությամբ և
                                                        առաջխաղացմամբ: Մենք հանդես ենք գալիս որպես օղակ հաճախորդի և նրա նոր
                                                        հաճախորդների միջև՝ հնարավորինս արտացոլելով ընկերության
                                                        ուշադրությունը կայքերի և դրա նպատակների վրա: Այնուամենայնիվ մենք
                                                        փնտրում ենք յուրահատուկ մոտեցում յուրաքանչյուր հաճախորդի նկատմամբ՝
                                                        փորձելով ստեղծել իրենց կայքը:
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-xs-12 col-md-6 wwai-item">
                                                <img class="right-img"
                                                     src="{{asset('asset/about/about.png')}}"
                                                     alt="Մենք տաղանդ ունենք տեխնոլոգիայի ապագան">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                 <div class="waveWrapper waveAnimation">
  <div class="waveWrapperInner bgTop">
    <div class="wave waveTop" style="background-image: url('http://front-end-noobs.com/jecko/img/wave-top.png')"></div>
  </div>
  <div class="waveWrapperInner bgMiddle">
    <div class="wave waveMiddle" style="background-image: url('http://front-end-noobs.com/jecko/img/wave-mid.png')"></div>
  </div>
  <div class="waveWrapperInner bgBottom">
    <div class="wave waveBottom" style="background-image: url('http://front-end-noobs.com/jecko/img/wave-bot.png')"></div>
  </div>
</div>
            @include('include.service')
        </div>
        <div class="heigtbr"></div>
     
        <div class="heigtbr"></div>
        <div class="aboutUs_About__oPDNP" style="padding: 0;">
            <div class="saying_Saying__eQe3M"><h2>Մի խելացի մարդ ասել է </h2><span
                    class="saying_headerText__u11qC">Ուժն է ծնում իրավունք...</span>
                <div class="MuiContainer-root MuiContainer-maxWidthMd">
                    <div class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2">
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6">
                            <div class="item_SayItem__3diEW"><p>Մեր ցանկությունները իհարկե կարևոր են, բայց ես եկա այն եզրակացության, որ ինչ էլ չցանկանանք միևնույն է ստանալու ենք այն, ինչին արժանի ենք․․․</p>
                                <h6>Անանուն</h6>

                            </div>
                        </div>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6">
                            <div class="item_SayItem__3diEW"><p>Իմաստ չունի վարձել խելացի մարդկանց, իսկ հետո ցուցում տալ նրանց, թե ինչ անել: Մենք վարձում ենք խելացի մարդկանց, որպեսզի նրանք ասեն, թե մենք ինչ անենք...

                                    </p><h6>Սթիվ Ջոբս</h6>

                            </div>
                        </div>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6">
                            <div class="item_SayItem__3diEW"><p>Ինչպես է նավը հաղթում այդքան շատ ալիքների: Ես հասկացա, որովհետև նավը ունի նպատակ, իսկ ալիքները ոչ․․․</p><h6>
                                    Ուինսթոն Չերչիլ
                                    </h6>
                            </div>
                        </div>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-sm-6">
                            <div class="item_SayItem__3diEW"><p>Ժամանակն է որոշում ժամանակը, իսկ երբ զգում ես, որ ժամանակն է, հասկանում ես, որ ժամանակը վաղուց արդեն անցել է..</p><h6>Թ.Թ</h6>
{{--                                <span class="item_date__24yMK">2020-06-25</span>--}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="heigtbr"></div>
        @include('include.contact')
    </div>
@endsection
@section('js')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<!--<script>-->
<!--               $('html, body').animate({-->
<!--        scrollTop: parseInt($("#loading").offset().top)-->
<!--    }, 10);-->

 
<!--       $('.bodys').fadeIn('slow'); -->
<!--        $(window).load(function() {-->
            
<!--   setTimeout(function(){ -->
<!--       $('.bodys').fadeOut('slow'); -->
<!--       $('.sticky').show(); -->
  
       
<!--   }, 400);-->
<!-- });-->
<!--</script>-->
@endsection
