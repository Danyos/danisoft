<div class="comment-box">
    <div class="comment">
        <div class="author-thumb"><img
                src="{{asset($blogComments->avatar)}}" alt=""></div>
        <div class="comment-info clearfix"><strong>{{$blogComments->name}}</strong>
            <div class="comment-time">{{$blogComments->created_at->format('m-d-Y h:i')}}</div>
        </div>
        <div class="text">{{$blogComments->message}}
        </div>
    </div>
</div>
