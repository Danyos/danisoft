@extends('admin.layouts.admin')

@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }}
        </div>

        <div class="card-body">
            <form action="{{ route("admin.campetition.store") }}" method="POST" enctype="multipart/form-data">
                @csrf



                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="title">Անուն</label>
                    <input type="text" id="title" name="name" class="form-control"  value="{{ old('name', isset($aboutinform) ? $aboutinform->name : '') }}" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                    <label for="link">Հղում </label>
                    <input type="url" name="link" id="link" required>
                    @if($errors->has('link'))
                        <em class="invalid-feedback">
                            {{ $errors->first('link') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatarLink') ? 'has-error' : '' }}">
                    <label for="avatarLink">Լուսանկարի հղում</label>
                    <input type="url" name="avatarLink" id="avatarLink" value="{{ old('avatarLink', isset($list) ? $list->avatarLink : '') }}" required>

                    @if($errors->has('avatarLink'))

                        <em class="invalid-feedback">
                            {{ $errors->first('avatarLink') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Հաստատել">
                </div>
            </form>
        </div>
    </div>

@endsection

