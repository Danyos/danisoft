@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Գլխավոր</a></li>
                <li class="breadcrumb-item active" aria-current="page">Մրցույթ </li>
            </ol>
        </nav>
    </div>
    @can('about_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.campetition.create") }}" target="_blank">
                Ավելացնել նոր մասնակից
            </a>
        </div>
    </div>
    @endcan
    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($list as $key => $inform)
                    <div class="col-md-6 col-lg-4">
                        <!-- Ibox -->
                        <div class="ibox bg-boxshadow mb-50">
                            <!-- Content -->
                            <div class="ibox-content blog">
                                <a href="">
                                    <h4>
                                        <a href="{{ $inform->link ?? '' }} ">
                                        {{ $inform->name ?? '' }}
                                        </a>
                                    </h4>
                                </a>
                                <p>  </p>

                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="blog-tag">Настройка:</h5>

                                        @can('about_edit')
                                        <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.campetition.edit',$inform->id)}}'">Редактировать</button>
                                        @endcan

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>



@endsection
