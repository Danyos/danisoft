@extends('admin.layouts.admin')

@section('content')

    <div class="card">
        <div class="card-header">
            Редактировать:
        </div>

        <div class="card-body">
            <form action="{{ route("admin.campetition.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="title">Անուն</label>
                    <input type="text" id="title" name="name" class="form-control" value="{{ old('name', isset($list) ? $list->name : '') }}" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                    <label for="description">Հղում </label>
                    <input type="url" name="link" id="link" value="{{ old('link', isset($list) ? $list->link : '') }}" required>

                @if($errors->has('link'))

                        <em class="invalid-feedback">
                            {{ $errors->first('link') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatarLink') ? 'has-error' : '' }}">
                    <label for="avatarLink">Լուսանկարի հղում</label>
                    <input type="url" name="avatarLink" id="avatarLink" value="{{ old('avatarLink', isset($list) ? $list->avatarLink : '') }}" required>

                @if($errors->has('avatarLink'))

                        <em class="invalid-feedback">
                            {{ $errors->first('avatarLink') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Հաստատել">
                </div>
            </form>
        </div>
    </div>
@endsection

