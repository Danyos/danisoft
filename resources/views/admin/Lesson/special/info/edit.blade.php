@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.specialInform.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие:</label>
                    <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($specialInform) ? $specialInform->title : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">description: </label>
                  @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                    <textarea id="name" name="description" class="form-control"  required>{{ old('description', isset($specialInform) ? $specialInform->description : '') }}</textarea>
                </div>
                <input type="hidden" name="parent_id" value="{{$specialInform->parent_id}}">

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>




@endsection

