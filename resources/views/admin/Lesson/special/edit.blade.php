@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.special.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')


                <div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
                    <label for="name">Պայմաներ</label>
                    <select name="data" id="">
                        @foreach($data as $data_lists)
                            <option value="{{$data_lists->slug}}" selected>{{$data_lists->slug}}</option>
                        @endforeach
                            @foreach($dataNot as $data_lists)
                            <option value="{{$data_lists->slug}}">{{$data_lists->slug}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                    <label for="name">logo*</label>
                    <input type="file" id="name" name="logo" class="form-control" >
                    @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Url բառ</label>
                    <input type="text" id="name" name="slug" class="form-control" value="{{ old('slug', isset($special) ? $special->slug : '') }}" onkeyup=" $(this).val($(this).val().replace(/ +?/g, ''));" required>
                    @if($errors->has('slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('slug') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Заглавие:</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($special) ? $special->name : '') }}" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('slogan') ? 'has-error' : '' }}">
                    <label for="slogan">Անուն slogan</label>
                    <input type="text" id="slogan" name="slogan" class="form-control" value="{{ old('slogan', isset($special) ? $special->slogan : '') }}" required>
                    @if($errors->has('slogan'))
                        <em class="invalid-feedback">
                            {{ $errors->first('slogan') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
                    <label for="name">Պայմաներ</label>
                    <select name="price_id" id="">
                        @foreach($price_list as $price_lists)
                            <option value="{{$price_lists->id}}" selected>{{$price_lists->slug}}</option>
                        @endforeach
                            @foreach($price_listNot as $price_lists)
                            <option value="{{$price_lists->id}}">{{$price_lists->slug}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
                    <label for="name">Ուսուցիչ</label>
                    <select name="trainer[]" id="" multiple>
                        @foreach($trainers as $trainer)
                            <option value="{{$trainer->id}}" selected>{{$trainer->name}}</option>
                        @endforeach
                            @foreach($trainersNot as $trainer)
                            <option value="{{$trainer->id}}">{{$trainer->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="name">Նկարագրություն</label>

                    <textarea id="description" name="description" class="form-control inputFields ncontent" required>{{ old('description', isset($special) ? $special->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" >
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>











                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>
    <img src="{{asset($special->avatar)}}" alt="">
    <img src="{{asset($special->logo)}}" alt="">
@endsection

@section('js')

    <script>
        $(function () {
            $("#date").datetime({value: '+1min'});
            $("#update_cmd").button();
            $("#status").buttonset();
            $("#top_fut").buttonset();
            $("#lang").buttonset();
            $("#catList").buttonset();
        });
        tinymce.init({
            selector: '.ncontent',
            menubar: false,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak fullscreen",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            relative_urls: false,
            image_advtab: true,
            filemanager_title: "Responsive Filemanager",
            file_picker_types: 'file image media',
            external_filemanager_path: "/admin/filemanager/",
            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | formatselect removeformat | fullscreen",
            toolbar2: "undo redo | cut copy paste pastetext | forecolor backcolor | searchreplace  | responsivefilemanager | image | media | link unlink  | code",
            file_picker_callback: function (cb, value, meta) {
                var width = window.innerWidth - 30;
                var height = window.innerHeight - 60;
                if (width > 1800) width = 1800;
                if (height > 1200) height = 1200;
                if (width > 600) {
                    var width_reduce = (width - 20) % 138;
                    width = width - width_reduce + 10;
                }
                var urltype = 2;
                if (meta.filetype == 'image') {
                    urltype = 1;
                }
                if (meta.filetype == 'media') {
                    urltype = 3;
                }
                let title = "RESPONSIVE FileManager";
                if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                    title = this.settings.filemanager_title;
                }
                let akey = "key";
                if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                    akey = this.settings.filemanager_access_key;
                }
                let sort_by = "";
                if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                    sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                }
                let descending = "false";
                if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                    descending = this.settings.filemanager_descending;
                }
                let fldr = "";
                if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                    fldr = "&fldr=" + this.settings.filemanager_subfolder;
                }
                let crossdomain = "";
                if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                    crossdomain = "&crossdomain=1";

// Add handler for a message from ResponsiveFilemanager
                    if (window.addEventListener) {
                        window.addEventListener('message', filemanager_onMessage, false);
                    } else {
                        window.attachEvent('onmessage', filemanager_onMessage);
                    }
                }
                tinymce.activeEditor.windowManager.open({
                    title: title,
                    file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                    width: width,
                    height: height,
                    resizable: true,
                    maximizable: true,
                    inline: 1
                }, {
                    setUrl: function (url) {
                        cb(url);
                    }
                });
            },
        });
    </script>


@endsection
