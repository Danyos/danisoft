@extends('admin.layouts.admin')
@section('content')


    <!-- Breadcrumb Area -->

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Դասավանդողներ</li>
            </ol>
        </nav>
    </div>

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            @can('news_create')
            <a class="btn btn-success" href="{{ route("admin.lesson.create") }}">
                Опубликовано
            </a>
            @endcan
        </div>
    </div>
    <!-- Wrapper -->
    <div class="wrapper wrapper-content blog">
        <div class="container-fluid">
            <div class="row">

                @foreach($lesson as $key => $lessons)
                <div class="col-md-6 col-lg-4">
                    <!-- Ibox -->
                    <div class="ibox bg-boxshadow mb-50">
                        <!-- Content -->
                        <div class="ibox-content blog">
                            <a >
                                <h4>{{ $lessons->title ?? '' }}</h4>
                            </a>
                            <!-- Date -->
                            <div class="blog-date">
                                <p><img src="{{asset($lessons->avatar)}}" alt="" style="height: 150px;" > </p>
                            </div>
                            <p>     <table border="1" width="100%">
                                <tr>
                                    <th>Հայտարարությունը</th>
                                    <td>{{ $lessons->status ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>URL</th>
                                    <td>{{ $lessons->slug ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>րոպե</th>
                                    <td>{{ $lessons->math ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Ամիս</th>
                                    <td>{{$lessons->time}}</td>
                                </tr>
                                <tr>
                                    <th>Online Անհատական</th>
                                    <td>{{$lessons->onlinePersonal}} դրամ</td>
                                </tr>
                                <tr>
                                    <th>Online Խմբակային</th>
                                    <td>{{$lessons->onlineGroup}} դրամ</td>
                                </tr>
                                <tr>
                                    <th>offline Անհատական</th>
                                    <td>{{$lessons->offlinePersonal}} դրամ</td>
                                </tr>
                                <tr>
                                    <th>offline Խմբակային</th>
                                    <td>{{$lessons->offlineGroup}} դրամ</td>
                                </tr>
                            </table>    </p>

                            <div class="row">
                                <div class="col-md-8">

                                    @can('news_edit')
                                    <button class="btn btn-white btn-xs blog" type="button" onclick="location.href='{{route('admin.lesson.edit',$lessons->id)}}'">Редактировать</button>
                                    @endcan
                                    @can('news_delete')
                                        <form action="{{ route('admin.lesson.destroy', $lessons->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn  btn-danger btn-xs blog" type="submit">Удалить</button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                      @endforeach

            </div>
        </div>
    </div>


@endsection
