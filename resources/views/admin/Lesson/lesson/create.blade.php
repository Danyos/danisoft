@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.lesson.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Url բառ</label>
                    <input type="text" id="name" name="slug" class="form-control" value="" onkeyup=" $(this).val($(this).val().replace(/ +?/g, ''));" required>
                    @if($errors->has('slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('slug') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Հայտարարությունը</label>
                    <select name="status" id="">
                        <option value="active" selected>Միացված</option>
                        <option value="inactive">Անջատված</option>
                    </select>
                    @if($errors->has('status'))
                        <em class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Վերնագիր</label>
                    <input type="text" id="name" name="title" class="form-control" value="" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label for="name">language</label>
                    <input type="text" id="name" name="language" class="form-control" value="" >
                    @if($errors->has('language'))
                        <em class="invalid-feedback">
                            {{ $errors->first('language') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="name">Նկարագրություն</label>

                    <textarea id="description" name="description" class="form-control" required></textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" required>
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('time') ? 'has-error' : '' }}">
                    <label for="time">րոպե</label>
                    <input type="text" id="time" name="time" class="form-control" value="" required>
                    @if($errors->has('time'))
                        <em class="invalid-feedback">
                            {{ $errors->first('time') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('math') ? 'has-error' : '' }}">
                    <label for="math">Ամիս</label>
                    <input type="text" id="math" name="math" class="form-control" value="" required>
                    @if($errors->has('math'))
                        <em class="invalid-feedback">
                            {{ $errors->first('math') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('onlinePersonal') ? 'has-error' : '' }}">
                    <label for="onlinePersonal">Online Անհատական</label>
                    <input type="text" id="onlinePersonal" name="onlinePersonal" class="form-control" value="" >
                    @if($errors->has('onlinePersonal'))
                        <em class="invalid-feedback">
                            {{ $errors->first('onlinePersonal') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('onlineGroup') ? 'has-error' : '' }}">
                    <label for="onlineGroup">Online Խմբակային</label>
                    <input type="text" id="onlineGroup" name="onlineGroup" class="form-control" value="" >
                    @if($errors->has('onlineGroup'))
                        <em class="invalid-feedback">
                            {{ $errors->first('onlineGroup') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('offlinePersonal') ? 'has-error' : '' }}">
                    <label for="offlinePersonal">offline Անհատական</label>
                    <input type="text" id="offlinePersonal" name="offlinePersonal" class="form-control" value="" >
                    @if($errors->has('offlinePersonal'))
                        <em class="invalid-feedback">
                            {{ $errors->first('offlinePersonal') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('offlineGroup') ? 'has-error' : '' }}">
                    <label for="offlineGroup">offline Խմբակային</label>
                    <input type="text" id="offlineGroup" name="offlineGroup" class="form-control" value="" >
                    @if($errors->has('offlineGroup'))
                        <em class="invalid-feedback">
                            {{ $errors->first('offlineGroup') }}
                        </em>
                    @endif
                </div>

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

