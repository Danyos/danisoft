@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.lesson.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')


                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Url բառ</label>
                    <input type="text" id="name" name="slug" class="form-control" value="{{ old('slug', isset($lesson) ? $lesson->slug : '') }}" onkeyup="$(this).val($(this).val().replace(/ +?/g, ''));" required>
                    @if($errors->has('slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('slug') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Հայտարարությունը</label>
                    <select name="status" id="">
                        @if($lesson->status=='active')
                        <option value="active" selected>Միացված</option>
                        <option value="inactive">Անջատված</option>
                        @else
                            <option value="active" >Միացված</option>
                            <option value="inactive" selected>Անջատված</option>
                        @endif

                    </select>
                    @if($errors->has('status'))
                        <em class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Վերնագիր</label>
                    <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($lesson) ? $lesson->title : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label for="name">language</label>
                    <input type="text" id="name" name="language" class="form-control" value="{{ old('language', isset($lesson) ? $lesson->language : '') }}" >
                    @if($errors->has('language'))
                        <em class="invalid-feedback">
                            {{ $errors->first('language') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="name">Նկարագրություն</label>

                    <textarea id="description" name="description" class="form-control" required>{{ old('description', isset($lesson) ? $lesson->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" >
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('time') ? 'has-error' : '' }}">
                    <label for="time">րոպե</label>
                    <input type="text" id="time" name="time" class="form-control" value="{{ old('time', isset($lesson) ? $lesson->time : '') }}" >
                    @if($errors->has('time'))
                        <em class="invalid-feedback">
                            {{ $errors->first('time') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('math') ? 'has-error' : '' }}">
                    <label for="math">Ամիս</label>
                    <input type="text" id="math" name="math" class="form-control" value="{{ old('math', isset($lesson) ? $lesson->math : '') }}" >
                    @if($errors->has('math'))
                        <em class="invalid-feedback">
                            {{ $errors->first('math') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('onlinePersonal') ? 'has-error' : '' }}">
                    <label for="onlinePersonal">Online Անհատական</label>
                    <input type="text" id="onlinePersonal" name="onlinePersonal" class="form-control" value="{{ old('onlinePersonal', isset($lesson) ? $lesson->onlinePersonal : '') }}" >
                    @if($errors->has('onlinePersonal'))
                        <em class="invalid-feedback">
                            {{ $errors->first('onlinePersonal') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('onlineGroup') ? 'has-error' : '' }}">
                    <label for="onlineGroup">Online Խմբակային</label>
                    <input type="text" id="onlineGroup" name="onlineGroup" class="form-control" value="{{ old('onlineGroup', isset($lesson) ? $lesson->onlineGroup : '') }}" >
                    @if($errors->has('onlineGroup'))
                        <em class="invalid-feedback">
                            {{ $errors->first('onlineGroup') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('offlinePersonal') ? 'has-error' : '' }}">
                    <label for="offlinePersonal">offline Անհատական</label>
                    <input type="text" id="offlinePersonal" name="offlinePersonal" class="form-control" value="{{ old('offlinePersonal', isset($lesson) ? $lesson->offlinePersonal : '') }}" >
                    @if($errors->has('offlinePersonal'))
                        <em class="invalid-feedback">
                            {{ $errors->first('offlinePersonal') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('offlineGroup') ? 'has-error' : '' }}">
                    <label for="offlineGroup">offline Խմբակային</label>
                    <input type="text" id="offlineGroup" name="offlineGroup" class="form-control" value="{{ old('offlineGroup', isset($lesson) ? $lesson->offlineGroup : '') }}" >
                    @if($errors->has('offlineGroup'))
                        <em class="invalid-feedback">
                            {{ $errors->first('offlineGroup') }}
                        </em>
                    @endif
                </div>












                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>
    <img src="{{asset($lesson->avatar)}}" alt="">
@endsection

