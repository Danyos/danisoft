@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.trainers.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Заглавие:</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($trainer) ? $trainer->name : '') }}" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
                    <label for="name">Մասնագիտություն</label>
                    <input type="text" id="name" name="profession" class="form-control" value="{{ old('profession', isset($trainer) ? $trainer->profession : '') }}" required>
                    @if($errors->has('profession'))
                        <em class="invalid-feedback">
                            {{ $errors->first('profession') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото:*</label>
                    <input type="file" id="name" name="avatar" class="form-control">
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>
    <img src="{{asset($trainer->avatar)}}" alt="">
@endsection

