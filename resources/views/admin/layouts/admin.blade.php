<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Colors">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>DaniSoft</title>
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link rel="icon" href="{{asset('assets/img/core-img/logo2.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/products-css/footable.css')}}">
@yield('css')
    <link rel="stylesheet" href="{{asset('assets/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

</head>

<body>
<div class="page-wrapper">

    <div class="page-top-bar-area d-flex align-items-center justify-content-between">
        <div class="logo-trigger-area d-flex align-items-center">

            <a href="{{route('admin.home')}}" class="logo">
                    <span class="big-logo">
                        <img src="{{asset('/assets/img/core-img/logo.png')}}" alt="">
                    </span>
                <span class="small-logo">
                        <img src="{{asset('/assets/img/core-img/logo2.png')}}" alt="">
                    </span>
            </a>


            <div class="top-trigger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>


        <div class="top-search-bar">
        </div>


        <div class="user-meta-data d-flex align-items-center">

            <!-- Language -->

            <!-- Profile -->
            <div class="topbar-profile">
                <!-- Thumb -->
                <div class="user---thumb">
                    <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->avatar)}}" alt="">
                </div>
                <!-- Profile Data -->
                <div class="profile-data">
                    <!-- Profile User Details -->
                    <div class="profile-user--details" style="background-image: url(img/thumbnails-img/profile-bg.jpg);">
                        <!-- Thumb -->
                        <div class="user---thumb">
                            <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->avatar)}}" style="ob" alt="">
                        </div>
                        <!-- Profile Text -->
                        <div class="profile---text-details">
                            <h6 style="color: black">{{\Illuminate\Support\Facades\Auth::user()->name}}</h6>
                            <a  style="color: black">{{\Illuminate\Support\Facades\Auth::user()->email}}</a>
                        </div>
                    </div>
                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="location.href='{{ route('admin.users.show', \Illuminate\Support\Facades\Auth::user()->id) }}'">
                            <h6>Мой профайл</h6>
                        </div>
                    </a>

                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="location.href='{{ route('admin.users.edit', \Illuminate\Support\Facades\Auth::user()->id) }}'">
                            <h6>Настройки учетной записи</h6>
                        </div>
                    </a>
                    <!-- Profile List Data -->

                    <!-- Profile List Data -->
                    <a class="profile-list--data" href="#">
                        <!-- Profile icon -->
                        <div class="profile--list-icon">
                            <i class="fa fa-sign-out text-danger" aria-hidden="true"></i>
                        </div>
                        <!-- Profile Text -->
                        <div class="notification--list-body-text profile" onclick="$('#logoutform').submit()">
                            <h6>Выход</h6>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- ###### Layout Container Area ###### -->
    <div class="layout-container-area mt-70">
        <!-- Side Menu Area -->
    @include('admin.partials.menu')

        <!-- Layout Container -->
        <div class="layout-container sidemenu-container mt-100">
@yield('content')
        </div>
    </div>
</div>

<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('assets2/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('assets/js/plugins-js/chart-js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/plugins-js/chart-js/chart-demo.js')}}"></script>

<script src="{{asset('assets/js/plugins-js/classy-nav.js')}}"></script>

<!-- Footable js -->
<script src="{{asset('assets/js/plugins-js/product-list-js/footable.js')}}"></script>

<!-- Active js -->
<script src="{{asset('assets/js/active.js')}}"></script>
<script  src="{{asset('assets2/tinymce/tinymce.min.js')}}"></script>
@yield('js')

</body>
</html>
