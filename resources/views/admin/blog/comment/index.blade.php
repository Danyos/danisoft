@extends('admin.layouts.admin')
@section('content')

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Գլխավո</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.blog.index')}}">Բլոգ</a></li>

            </ol>
        </nav>
    </div>



    <div class="wrapper wrapper-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!-- ibox -->
                    <div class="ibox bg-boxshadow">
                        <!-- ibox Content -->
                        <div class="ibox-content">
                            <div class="articals-date-btn d-sm-flex mb-30">
                                <div class="text-muted"><i class="fa fa-clock-o"></i> {!! $blognews->created_at->format('d-m-Y H:i') !!}</div>

                            </div>
                            <!-- Title -->
                            <div class="article-title">
                                <h2 class="text-center">  {!! $blognews->name_am !!}</h2>
                            </div>
                       {!! $blognews->info_am !!}


                            <img src="{{asset($blognews->avatar)}}" alt="">
                            <div class="row">
                                <!-- Comments -->
                                <div class="col-lg-12">
                                    <div class="articles-heading mt-30">
                                        <h2>Comments:</h2>
                                    </div>
                                    <!-- Feed Box -->
                                    <div class="oks col-lg-12">


                                        @foreach($blog->reverse() as $comments)
                                            <div class="article-social-feed-box id-{{$comments->id}}">
                                                <!-- Social Avatar -->
                                                <div class="social-avatar">
                                                    <a href="#" class="social-image"><img src="img/thumbnails-img/blog-1.jpg" alt=""></a>
                                                    <!-- Media Body -->
                                                    <div class="media-body">
                                                        <a href="#">Andrew Williams,</a>
                                                        <small>Today 4:21 pm - 12.06.2018</small>



                                                        <input type="hidden" name="id" id="comments_id" value="{{$comments->id}}">
                                                        <button class="btn  btn-danger btn-xs blog" type="button" onclick="deletecommens()">Ջնջել</button>

                                                    </div>
                                                </div>
                                                <!-- Social Body -->
                                                <div class="social-body">
                                                    <p>{{$comments->message}}</p>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                    <form action="javascript::void(0)" method="POST" id="ffre">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                    <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                        <label for="description">Նկարագրութհուն</label>
                                        <textarea id="description" name="message" class="form-control" style="border: 1px solid black;">{{ old('message') }}</textarea>
                                        <input type="hidden" name="blog_id" id="blog_id" value="{{$id}}">

                                    </div>
                                    <div>
                                        <input class="btn btn-danger jutak" type="button" value="Հաստատել" onclick="getUserAccount()">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>














    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        function getUserAccount() {
            var msg=document.getElementById('description').value;
            var blog=document.getElementById('blog_id').value;
            axios.post('{{route('admin.bcomments.store')}}',{'msg': msg,'blog':blog})
                .then(function (response) {

                    $('.oks').append(response.data)
                })
        }
        function deletecommens() {

            var id=document.getElementById('comments_id').value;

            axios.post('{{route('admin.comment.delete')}}',{'id': id})
                .then(function (response) {
                    console.log(response.data);
                    $('.id-'+response.data).remove().hide();
                })
        }

    </script>
@endsection
