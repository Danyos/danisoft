@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.blog.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="name">Url բառ</label>
                    <input type="text" id="slugchek" pattern="[^'\x22]+" name="slug" class="form-control col-3" value=""   onkeyup=" $(this).val($(this).val().replace(/ +?/g, ''));" required>
                 <span id="msg"></span>
                    @if($errors->has('slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('slug') }}
                        </em>
                    @endif
                    <button type="button" class="chek">Ստուգել Բառը</button>
                </div>
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="description">Youtube Link</label>
                    <br>
                    <input type="url" name="url" value="" class="form-control">
                    @if($errors->has('url'))
                        <em class="invalid-feedback">
                            {{ $errors->first('url') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('auth_offer') ? 'has-error' : '' }}" >
                    <label for="description">предложение авторизации пользователя</label>
                    <br>
                    <textarea name="auth_offer" class="form-control" required></textarea>
                    @if($errors->has('auth_offer'))
                        <em class="invalid-feedback">
                            {{ $errors->first('auth_offer') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Заглавие</label>
                    <input type="text" id="name" name="name" class="form-control" value="" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('info_ru') ? 'has-error' : '' }}">
                    <label for="description">Описание </label>
                    <textarea id="description" name="info" class="form-control inputFields ncontent" ></textarea>
                    @if($errors->has('info'))
                        <em class="invalid-feedback">
                            {{ $errors->first('info') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" required>
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger cheks" type="button" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection
@section('js')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script>

        $('.chek').click(function (){
            var dataval= $('#slugchek').val()
if (dataval==''){
    return false
}
            $.ajaxSetup({
                headers : { "X-CSRF-TOKEN" :jQuery(`meta[name="csrf-token"]`). attr("content")}
            });
            $.ajax({

                url: '{{route('admin.chek.slug')}}',
                type: "get",
                data: {'slug': dataval},

                success: function (response) {
if (response==1){
    $('#msg').html('noooooo');
    $('.cheks').attr('type', 'button');
}else{
    $('#msg').html('ok')
    $('.cheks').attr('type', 'submit');
}


                }

            });
        })
        $(function () {
            $("#date").datetime({value: '+1min'});
            $("#update_cmd").button();
            $("#status").buttonset();
            $("#top_fut").buttonset();
            $("#lang").buttonset();
            $("#catList").buttonset();
        });
        tinymce.init({
            selector: '.ncontent',
            menubar: false,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak fullscreen",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            relative_urls: false,
            image_advtab: true,
            filemanager_title: "Responsive Filemanager",
            file_picker_types: 'file image media',
            external_filemanager_path: "/admin/filemanager/",
            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | formatselect removeformat | fullscreen",
            toolbar2: "undo redo | cut copy paste pastetext | forecolor backcolor | searchreplace  | responsivefilemanager | image | media | link unlink  | code",
            file_picker_callback: function (cb, value, meta) {
                var width = window.innerWidth - 30;
                var height = window.innerHeight - 60;
                if (width > 1800) width = 1800;
                if (height > 1200) height = 1200;
                if (width > 600) {
                    var width_reduce = (width - 20) % 138;
                    width = width - width_reduce + 10;
                }
                var urltype = 2;
                if (meta.filetype == 'image') {
                    urltype = 1;
                }
                if (meta.filetype == 'media') {
                    urltype = 3;
                }
                let title = "RESPONSIVE FileManager";
                if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                    title = this.settings.filemanager_title;
                }
                let akey = "key";
                if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                    akey = this.settings.filemanager_access_key;
                }
                let sort_by = "";
                if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                    sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                }
                let descending = "false";
                if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                    descending = this.settings.filemanager_descending;
                }
                let fldr = "";
                if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                    fldr = "&fldr=" + this.settings.filemanager_subfolder;
                }
                let crossdomain = "";
                if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                    crossdomain = "&crossdomain=1";

                    // Add handler for a message from ResponsiveFilemanager
                    if (window.addEventListener) {
                        window.addEventListener('message', filemanager_onMessage, false);
                    } else {
                        window.attachEvent('onmessage', filemanager_onMessage);
                    }
                }
                tinymce.activeEditor.windowManager.open({
                    title: title,
                    file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                    width: width,
                    height: height,
                    resizable: true,
                    maximizable: true,
                    inline: 1
                }, {
                    setUrl: function (url) {
                        cb(url);
                    }
                });
            },
        });
    </script>


@endsection
