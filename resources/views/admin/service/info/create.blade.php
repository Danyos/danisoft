@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.service_info.store") }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие</label>
                    <input type="text" id="name" name="title" class="form-control" value="" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                    <label for="description">price </label>
                    <input type="text" id="name" name="price" class="form-control" value="" >
                    @if($errors->has('price'))
                        <em class="invalid-feedback">
                            {{ $errors->first('price') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('href') ? 'has-error' : '' }}">
                    <label for="description">հղում </label>
                    <input type="text" id="name" name="href" class="form-control" value="" >
                    @if($errors->has('href'))
                        <em class="invalid-feedback">
                            {{ $errors->first('href') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="name">Նկարագրություն</label>

                    <textarea id="description" name="description" class="form-control" ></textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <input type="hidden" name="parent_id" value="{{$id}}">

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

