@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.service_info.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие:</label>
                    <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($service) ? $service->title : '') }}" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">price: </label>
                    <input type="text" id="name" name="price" class="form-control" value="{{ old('price', isset($service) ? $service->price : '') }}" >
                @if($errors->has('price'))
                        <em class="invalid-feedback">
                            {{ $errors->first('price') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('href') ? 'has-error' : '' }}">
                    <label for="description">հղում </label>
                    <input type="text" id="name" name="href" class="form-control" value="{{ old('href', isset($service) ? $service->href : '') }}" >
                    @if($errors->has('href'))
                        <em class="invalid-feedback">
                            {{ $errors->first('href') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="name">Նկարագրություն</label>

                    <textarea id="description" name="description" class="form-control" >{{ old('description', isset($service) ? $service->description : '') }}</textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>
                <input type="hidden" name="parent_id" value="{{$service->parent_id}}">

                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>




@endsection

