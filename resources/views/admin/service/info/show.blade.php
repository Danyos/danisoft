@extends('admin.layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Наши услуги
        </div>
        <h1 align="center">    {{ $service->title }}</h1>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>

                <tr>
                    <th>
                        Заглавие
                    </th>
                    <td>
                        {{ $service->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Описание
                    </th>
                    <td>
                        {{ $service->price }}
                    </td>
                </tr>
                @foreach($package as $packagess)
                <tr>
                    <th>
                        предложение авторизации пользователя
                    </th>
                    <td>
                        {{ $packagess->title }}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>

@endsection
