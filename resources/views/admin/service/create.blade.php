@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.service.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="description">Youtube Link</label>
                    <br>
                    <input type="url" name="url" value="" class="form-control">
                    @if($errors->has('url'))
                        <em class="invalid-feedback">
                            {{ $errors->first('url') }}
                        </em>
                    @endif
                </div>



                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие</label>
                    <input type="text" id="name" name="title" class="form-control" value="" required>
                    @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Описание </label>
                    <textarea id="description" name="description" class="form-control " required></textarea>
                    @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото*</label>
                    <input type="file" id="name" name="avatar" class="form-control" required>
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('offer') ? 'has-error' : '' }}" >
                    <label for="description">предложение авторизации пользователя</label>
                    <br>
                    <textarea name="offer" class="form-control" required></textarea>
                    @if($errors->has('offer'))
                        <em class="invalid-feedback">
                            {{ $errors->first('offer') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>

@endsection

