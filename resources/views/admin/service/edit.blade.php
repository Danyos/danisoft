@extends('admin.layouts.admin')

@section('content')

    <div class="card">


        <div class="card-body">
            <form action="{{ route("admin.service.update",$id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="description">Youtube Link</label>
                    <br>
                    <input type="url" name="url" value="{{ old('url', isset($service) ? $service->url : '') }}" class="form-control" >
                    @if($errors->has('url'))
                        <em class="invalid-feedback">
                            {{ $errors->first('url') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">Заглавие:</label>
                    <input type="text" id="name" name="title" class="form-control" value="{{ old('name', isset($service) ? $service->title : '') }}" required>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('info_ru') ? 'has-error' : '' }}">
                    <label for="description">Описание: </label>
                    <textarea id="description" name="description" class="form-control inputFields ncontent" required>{{ old('description', isset($service) ? $service->description : '') }}</textarea>
                    @if($errors->has('info'))
                        <em class="invalid-feedback">
                            {{ $errors->first('info') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Фото:*</label>
                    <input type="file" id="name" name="avatar" class="form-control">
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('offer') ? 'has-error' : '' }}">
                    <label for="description">предложение </label>
                    <br>
                    <textarea name="offer" class="form-control" required>{{ old('offer', isset($service) ? $service->offer : '') }}</textarea>
                    @if($errors->has('auth_offer'))
                        <em class="invalid-feedback">
                            {{ $errors->first('auth_offer') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="Подтвердите">
                </div>
            </form>
        </div>
    </div>



    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-lg-3">
                <!-- Widget Content -->
                <div class="widget-content-style rounded-0 one zoom-effect mb-30">
                    <div class="row">
                        <img src="{{asset($service->avatar)}}" alt="">
                    </div>
                </div>
            </div>




            <div class="col-sm-6 col-lg-4">
                <!-- Widget Content -->
                <div class="widget-content-style lazur-bg zoom-effect mb-30">
                    <div class="row">
                        <iframe class="main-video-frame" src="{{ old('url', isset($service) ? $service->url : '') }}" frameborder="0" width="100%"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $(function () {
            $("#date").datetime({value: '+1min'});
            $("#update_cmd").button();
            $("#status").buttonset();
            $("#top_fut").buttonset();
            $("#lang").buttonset();
            $("#catList").buttonset();
        });
        tinymce.init({
            selector: '.ncontent',
            menubar: false,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak fullscreen",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            relative_urls: false,
            image_advtab: true,
            filemanager_title: "Responsive Filemanager",
            file_picker_types: 'file image media',
            external_filemanager_path: "/admin/filemanager/",
            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | formatselect removeformat | fullscreen",
            toolbar2: "undo redo | cut copy paste pastetext | forecolor backcolor | searchreplace  | responsivefilemanager | image | media | link unlink  | code",
            file_picker_callback: function (cb, value, meta) {
                var width = window.innerWidth - 30;
                var height = window.innerHeight - 60;
                if (width > 1800) width = 1800;
                if (height > 1200) height = 1200;
                if (width > 600) {
                    var width_reduce = (width - 20) % 138;
                    width = width - width_reduce + 10;
                }
                var urltype = 2;
                if (meta.filetype == 'image') {
                    urltype = 1;
                }
                if (meta.filetype == 'media') {
                    urltype = 3;
                }
                let title = "RESPONSIVE FileManager";
                if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                    title = this.settings.filemanager_title;
                }
                let akey = "key";
                if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                    akey = this.settings.filemanager_access_key;
                }
                let sort_by = "";
                if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                    sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                }
                let descending = "false";
                if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                    descending = this.settings.filemanager_descending;
                }
                let fldr = "";
                if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                    fldr = "&fldr=" + this.settings.filemanager_subfolder;
                }
                let crossdomain = "";
                if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                    crossdomain = "&crossdomain=1";

                    // Add handler for a message from ResponsiveFilemanager
                    if (window.addEventListener) {
                        window.addEventListener('message', filemanager_onMessage, false);
                    } else {
                        window.attachEvent('onmessage', filemanager_onMessage);
                    }
                }
                tinymce.activeEditor.windowManager.open({
                    title: title,
                    file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                    width: width,
                    height: height,
                    resizable: true,
                    maximizable: true,
                    inline: 1
                }, {
                    setUrl: function (url) {
                        cb(url);
                    }
                });
            },
        });
    </script>


@endsection
