@extends('admin.layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Наши услуги
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th>
                        Фото
                    </th>
                    <td>
                        <img src="{{asset($service->avatar)}}" alt="">
                    </td>
                </tr>
                <tr>
                    <th>
                        Заглавие
                    </th>
                    <td>
                        {{ $service->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Описание
                    </th>
                    <td>
                        {{ $service->description }}
                    </td>
                </tr>
                <tr>
                    <th>
                        предложение авторизации пользователя
                    </th>
                    <td>
                        {{ $service->offer }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Youtube Link
                    </th>
                    <td>
                        <iframe class="main-video-frame" src="{{ old('url', isset($service) ? $service->url : '') }}" frameborder="0" width="100%"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </td>
                </tr>
                </tbody>
            </table>


@endsection
