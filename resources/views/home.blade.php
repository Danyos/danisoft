@extends('admin.layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-lg-3">
                <!-- Widget Content -->
                <div class="widget-content-style rounded-0 one zoom-effect mb-30">
                    <div class="row">
                        <div class="col-4">
                            <i class="icon_genius fa-5x"></i>>
                        </div>
                        @can('service_access')
                        <div class="col-8 text-right" onclick="location.href='{{route('admin.service.index')}}'">
                            <span> НАШИ УСЛУГИ </span>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>




            <div class="col-sm-6 col-lg-4">
                <!-- Widget Content -->
                <div class="widget-content-style lazur-bg zoom-effect mb-30">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        @can('contact_accses')
                        <div class="col-8 text-right" onclick="location.href='{{route('admin.contact.index')}}'">
                            <span> Новое Сообщения</span>
                            <h2 class="widget-content--text color-white">{{$contact}}</h2>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <!-- Widget Content -->
                <div class="widget-content-style yellow-bg zoom-effect mb-30">
                    <div class="row">
                        <div class="col-4">
                            <i class="icon_pencil-edit fa-5x"></i>
                        </div>
                        @can('news_access')
                        <div class="col-8 text-right" onclick="location.href='{{route('admin.blog.index')}}'">
                            <span> Блог </span>
                            <h2 class="widget-content--text color-white">{{$blog->count()}}</h2>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>

        </div>
    </div>
    <h2 align="center">  Блог</h2>
    <div class="wrapper wrapper-content">
        <div class="container-fluid">
            <div class="row">

                @foreach($blog as $blogs)
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Notes -->
                    <div class="notes-pin-board mb-50">
                        <!-- Content -->
                        <div class="pin-board-content">
                            <small>{{$blogs->created_at->format(' M d Y H:i')}}</small>
                            <h4>{{$blogs->name}}</h4>
                            <p>      {{ mb_strlen( strip_tags($blogs->info, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($blogs->info, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($blogs->info, '<b><i><u>') }}
                            </p>
                            @can('news_edit')
                            <a href="{{route('admin.blog.edit',$blogs->id)}}"><i class="fa fa-edit"></i></a>
                            @endcan


                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
@section('scripts')
@parent

@endsection
