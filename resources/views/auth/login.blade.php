<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- styles -->
    <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
    <title>Login</title>
    <style>
        h1, h2, h3, h4, h5, h6 {
        }
        section {
            padding: 60px 0;
            min-height: 100vh;
        }
        a, a:hover, a:focus, a:active {
            text-decoration: none;
            outline: none;
        }
        ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .bg-gradient {
            background: #393e9e;
            background: -webkit-linear-gradient(to right, #797cd2, #393e9e);
            background: linear-gradient(to right, #797cd2, #393e9e);
        }

        .bg-overlay-gradient {
            background: #393e9e;
            background: -webkit-linear-gradient(to right, #797cd2, #393e9e);
            background: linear-gradient(to right, #797cd2, #393e9e);
            opacity: 0.9;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
        }

        .home-table-center {
            display: table-cell;
            vertical-align: middle;
        }

        .home-table {
            display: table;
            width: 100%;
            min-height: 100vh;
            height: 100%;
        }

        .header_title {
            font-size: 44px;
            line-height: 1.2;
            max-width: 850px;
            text-transform: capitalize;
        }

        .small_title p {
            font-size: 16px;
            border-radius: 30px;
            padding: 4px 18px;
            background-color: rgba(255, 255, 255, 0.1);
            display: inline-block;
        }

        .header_subtitle {
            line-height: 1.8;
            max-width: 450px;
            color: rgba(255, 255, 255, 0.6) !important;
        }

        .home-desk {
            position: relative;
            top: 60px;
            z-index: 100;
        }


        .btn-custom {
            background-color: #0ebdca;
            border: 2px solid #0ebdca;
            color: #fff;
            font-size: 14px;
            transition: all 0.5s;
            border-radius: 5px;
            letter-spacing: 1px;
            text-transform: capitalize;
        }
        .btn-custom:hover{
            opacity: 0.8;
        }
        .account_box{
            border-radius: 12px;
            box-shadow: 10px -10px 0 4px #0ebdca;
            padding: 50px 40px;
        }

        .account_box h5 {
            font-size: 16px;
            max-width: 100%;
            line-height: 1.4;
        }
        .account_box .form-control{
            box-shadow: none !important;
            color: #fff;
            height: 46px;
            border: none;
            border-radius: 3px;
            font-size: 14px;
            background-color: rgba(255, 255, 255, 0.1);
        }

        .account_box .form-control::-webkit-input-placeholder {
            color: rgba(255, 255, 255, 0.6);
        }

        .account_box .form-control::-moz-placeholder {
            color: rgba(255, 255, 255, 0.6);
        }

        .account_box .form-control:-ms-input-placeholder {
            color: rgba(255, 255, 255, 0.6);
        }

        .account_box .form-control:-moz-placeholder {
            color: rgba(255, 255, 255, 0.6);
        }

    </style>
</head>
<body>

<div class="bg-gradient position-relative h-100vh">
    <div class="home-table">
        <div class="home-table-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="text-center">
                            <p class="mb-4 pb-3"><a href="{{route('index')}}" class="text-white">Վերադառնալ գլխավոր<i class="mdi mdi-home"></i></a></p>
                        </div>
                        <div class="account_box bg-gradient">
                            @if(\Session::has('message'))
                                <p class="alert alert-info">
                                    {{ \Session::get('message') }}
                                </p>
                            @endif
                            <img src="{{asset('asset/images/danisoft.png')}}" alt="" class="img-fluid mx-auto d-block" style="height: 142px;    margin-bottom: 15px;">
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="col-lg-12 mt-3">
                                    <label class="text-white">Փոստ</label>
                                    <input type="email" id="exampleInputEmail1" class="form-control trial-input" placeholder="Փոստ" name="email">
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <label class="text-white">Գաղտնաբառ</label>
                                    <input type="password" name="password" id="password1" class="form-control trial-input" placeholder="Գաղտնաբառ">
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1"  name="remember" >
                                        <label class="custom-control-label text-white" for="customCheck1">Հիշել ինձ</label>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button class="btn btn-custom w-100 mt-3">Մուտք</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
