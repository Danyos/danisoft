$(function() {
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
        function(e) {
            e.preventDefault();
            $(this).closest('.form-inline').remove();
        }
    );
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
        function(e) {
            e.preventDefault();
            var container = $(this).closest('[data-role="dynamic-fields"]');
            new_field_group = container.children().filter('.form-inline:first-child').clone();
            new_field_group.find('label').html('Upload Document'); new_field_group.find('input').each(function(){
                $(this).val('');
            });
            container.append(new_field_group);
        }
    );
});



// file upload

$(document).on('change', '.file-upload', function(){
    var i = $(this).prev('label').clone();
    var file = this.files[0].name;
    $(this).prev('label').text(file);
});
$(function() {
    $('#list').addInputArea({

        btn_add: '.add_list',
        btn_del: '.del-list'
    });
});
$(function() {
    $('#menu_window_show').addInputArea({

        area_var: '.menu_item',
        btn_add: '.menu_window_add',
        btn_del: '.menu_window_del'
    });
});
$(function() {
    $('#color').addInputArea({

        area_var: '.colorfilter',
        btn_add: '.add_colorfilter',
        btn_del: '.del_colorfilter'
    });
});  $(function() {
    $('#size').addInputArea({

        area_var: '.var_area02',
        btn_add: '.add_button02',
        btn_del: '.del_button02'
    });
});
$(function() {
    $('#length').addInputArea({

        area_var: '.var_length02',
        btn_add: '.add_length02',
        btn_del: '.del_length02'
    });
});
$(function() {
    $('#info').addInputArea({

        area_var: '.var_info02',
        btn_add: '.add_info02',
        btn_del: '.del_info02'
    });
});
$(function() {
    $('#info-images').addInputArea({

        area_var: '.var_images',
        btn_add: '.add_images',
        btn_del: '.del_images'
    });
});
function previewimage(event) {
    var output=document.getElementById('output');
    output.src=URL.createObjectURL(event.target.files[0]);
}
