<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogModels;
use App\Models\SliderModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider=SliderModel::get();
        return view('admin.slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('slider/');
            $pah = public_path('slider/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            SliderModel::create([

                'alt'=>$request['alt'],
                'avatar'=>'slider/'.$imagename,
            ]);

            return redirect()->route('admin.slider.index');
        }else{

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=SliderModel::find($id)->delete();
        return  back();
    }
}
