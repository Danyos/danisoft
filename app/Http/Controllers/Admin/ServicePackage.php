<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicePackage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function creates($id)
    {

        return view('admin.service.info.package.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $service=\App\Models\ServicePackage::create($request->all());

        return redirect()->route('admin.service_Package.show',$request->child_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $service = \App\Models\ServicePackage::where('child_id',$id)->get();

        if ($service->count() > 0):
            return view('admin.service.info.package.index', compact('id','service'));
        else:
            return view('admin.service.info.package.create', compact('id'));

        endif;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package=\App\Models\ServicePackage::find($id);
        return view('admin.service.info.package.edit', compact('id','package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $service=\App\Models\ServicePackage::find($id)->update($request->all());

        return redirect()->route('admin.service_Package.show',$request->child_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package=\App\Models\ServicePackage::find($id);
        $k=$package->child_id;
        $package->delete();

              return redirect()->route('admin.service_Package.show',$k);
    }
}
