<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServiceInformateModels;
use App\Models\ServiceModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceInformateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.service.info.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        ServiceInformateModels::create($request->all());

        return redirect()->route('admin.service_info.show', $request->parent_id);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $service = ServiceInformateModels::where('parent_id', $id)->get();
        if ($service->count() > 0):
            return view('admin.service.info.index', compact('id','service'));
        else:
            return view('admin.service.info.create', compact('id'));

        endif;

        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = ServiceInformateModels::find($id);

        return view('admin.service.info.edit', compact('id', 'service'));
    }
    public function view($id)
    {
        $service = ServiceInformateModels::find($id);
        $package=\App\Models\ServicePackage::where('child_id',$id)->get();

        return view('admin.service.info.show', compact('id', 'service','package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ServiceInformateModels::find($id)->update($request->all());
        return redirect()->route('admin.service.show', $request->parent_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceInformateModels::find($id)->delete();
        return back();
    }
}
