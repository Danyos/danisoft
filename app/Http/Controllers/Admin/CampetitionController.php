<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompetitionList;
use App\Models\CompetitionModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $list=CompetitionModel::get();
        return  view('admin.Competition.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.Competition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        CompetitionModel::create($request->all());
        return redirect()->route('admin.campetition.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        CompetitionList::whereNotNull('id')->delete();
        $list=CompetitionModel::inRandomOrder()->get();
        return  view('admin.Competition.show',compact('list','id'));
    }
      public function ajaxRequest($id)
    {
        $listCount=CompetitionList::get()->count();
        $first=CompetitionList::first();
//        if ($listCount==0){
//            $list=CompetitionModel::find(11);
//
//            CompetitionList::create([
//                'parent_id'=>11,
//                'to'=>$id,
//            ]);
//
//        }else{
//            $list=CompetitionModel::where('id','!=',11)->find($id);
            $list=CompetitionModel::find($id);
            if ($list){
                CompetitionList::create([
                    'parent_id'=>$id
                ]);
            }else{
                $list=CompetitionModel::find($first->to);
                CompetitionList::create([
                    'parent_id'=>$first->to
                ]);
            }


//        }
        return  $list;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list=CompetitionModel::find($id);
        return  view('admin.Competition.edit',compact('list','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CompetitionModel::find($id)->update($request->all());
        return redirect()->route('admin.campetition.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function result()
    {
        $first1=CompetitionList::first();
        $first2=CompetitionList::skip(1)->take(2)->get();

        $first3=CompetitionList::skip(3)->take(3)->get();
        return  view('admin.Competition.include.horizonakan',compact('first1','first2','first3'));
    }
    public function trashCache()
    {
        CompetitionList::whereNotNull('id')->delete();

        return back();
    }
}
