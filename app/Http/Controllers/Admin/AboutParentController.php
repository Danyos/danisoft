<?php

namespace App\Http\Controllers\Admin;

use App\Models\AboutParentModels;
use App\Models\BlogModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminControoler;
use Intervention\Image\Facades\Image;

class AboutParentController extends AdminControoler
{
    public function index(){
        $about=AboutParentModels::first();
        return view('admin.about.parent',compact('about'));
    }
    public function update(Request $request, $id)
    {

        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('about/');
            $pah = public_path('about/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            AboutParentModels::first()->update([
                'title'=>$request['title'],
                'description'=>$request['description'],
                'url'=>$request['url'],
                'avatar'=>'about/'.$imagename,
            ]);

            return redirect()->route('admin.about.index');
        }else{
            AboutParentModels::first()->update([
                'title'=>$request['title'],
                'description'=>$request['description'],
                'url'=>$request['url'],

            ]);
            return redirect()->route('admin.about.index');
        }
    }
}
