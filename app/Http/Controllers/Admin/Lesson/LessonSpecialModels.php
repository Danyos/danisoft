<?php

namespace App\Http\Controllers\Admin\Lesson;

use App\Models\Lesson\LessonModels;
use App\Models\Lesson\LessonParentModels;
use App\Models\Lesson\LessonPriceListModels;
use App\Models\Lesson\LessonTrainersIdGroupsModels;
use App\Models\Lesson\LessonTrainersModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class LessonSpecialModels extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $special=LessonParentModels::get();

        return view('admin.Lesson.special.index',compact('special'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=LessonModels::where('more','active')->get();
        $price_list=LessonPriceListModels::get();
        $trainers=LessonTrainersModels::get();

        return view('admin.Lesson.special.create',compact('price_list','trainers','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->has('logo')) {

            $imagenameLogo = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->logo->getClientOriginalExtension();

            $destinationPaths = public_path('special/logo/');
            $pah = public_path('special/logo/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_imgs = Image::make($request->logo->getRealPath())->encode('jpg', 75);
            $thumb_imgs->save($destinationPaths . '/' . $imagenameLogo, 80);
        }else{

            return back()->withInput();

        }
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('special/');
            $pah = public_path('special/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);
        }else{

            return back()->withInput();

        }

            $datas=LessonParentModels::create([
                'slug'=>$request['slug'],
                'name'=>$request['name'],
                'data'=>$request['data'],
                'description'=>$request['description'],
                'price_id'=>$request['price_id'],
                'slogan'=>$request['slogan'],
                'avatar'=>'special/'.$imagename,
                'logo'=>'special/logo/'.$imagenameLogo,
            ]);
        foreach ($request['trainer'] as $teachers){
            LessonTrainersIdGroupsModels::create([
                'lesson_id'=>$datas->id,
                'trainers_id'=>$teachers,
]);
        }


            return redirect()->route('admin.special.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special=LessonParentModels::find($id);
        $dataNot=LessonModels::where('slug','!=',$special->data)->where('more','active')->get();
        $data=LessonModels::where('slug','=',$special->data)->where('more','active')->get();
        $price_list=LessonPriceListModels::where('id','=',$special->price_id)->get();
        $price_listNot=LessonPriceListModels::where('id','!=',$special->price_id)->get();

        $group=LessonTrainersIdGroupsModels::where('lesson_id',$special->id)->get('trainers_id')->toArray();

        $trainers=LessonTrainersModels::whereIn('id',$group)->get();
        $trainersNot=LessonTrainersModels::whereNotIn('id',$group)->get();




        return view('admin.Lesson.special.edit',compact('special',
            'id','data','price_list','trainers','price_listNot','dataNot','trainers','trainersNot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datas=LessonParentModels::find($id);
        if ($request->has('logo')) {

            $imagenameLogo = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->logo->getClientOriginalExtension();

            $destinationPaths = public_path('special/logo/');
            $pah = public_path('special/logo/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_imgs = Image::make($request->logo->getRealPath())->encode('jpg', 75);
            $thumb_imgs->save($destinationPaths . '/' . $imagenameLogo, 80);
            $logo='special/logo/'.$imagenameLogo;
        }
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('special/');
            $pah = public_path('special/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);
            $avatar='special/'.$imagename;
        }
        $datas->update([
            'slug'=>$request['slug'],
            'name'=>$request['name'],
            'data'=>$request['data'],
            'description'=>$request['description'],
            'price_id'=>$request['price_id'],
            'slogan'=>$request['slogan'],
            'avatar'=>$avatar??$datas->avatar,
            'logo'=>$logo??$datas->logo,
        ]);

        LessonTrainersIdGroupsModels::where('lesson_id',$datas->id)->delete();

        foreach ($request['trainer'] as $teachers){
            LessonTrainersIdGroupsModels::create([
                'lesson_id'=>$datas->id,
                'trainers_id'=>$teachers,
            ]);
        }

        return redirect()->route('admin.special.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $special=LessonParentModels::find($id)->delete();
        return back();
    }
}
