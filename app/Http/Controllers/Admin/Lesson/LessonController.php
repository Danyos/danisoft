<?php

namespace App\Http\Controllers\Admin\Lesson;

use App\Models\Lesson\LessonModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesson=LessonModels::get();

        return view('admin.Lesson.lesson.index',compact('lesson'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Lesson.lesson.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('lesson/');
            $pah = public_path('lesson/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            LessonModels::create([

                'slug'=>$request['slug'],
                'title'=>$request['title'],
                'language'=>$request['language'],
                'description'=>$request['description'],
                'status'=>$request['status'],
                'time'=>$request['time'],
                'math'=>$request['math'],
                'onlinePersonal'=>$request['onlinePersonal'],
                'onlineGroup'=>$request['onlineGroup'],
                'offlinePersonal'=>$request['offlinePersonal'],
                'offlineGroup'=>$request['offlineGroup'],
                'avatar'=>'lesson/'.$imagename,
            ]);

            return redirect()->route('admin.lesson.index');
        }else{

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson=LessonModels::find($id);

        return view('admin.Lesson.lesson.edit',compact('lesson','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('lesson/');
            $pah = public_path('lesson/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            LessonModels::find($id)->update([
                'slug'=>$request['slug'],
                'title'=>$request['title'],
                'language'=>$request['language'],
                'description'=>$request['description'],
                'status'=>$request['status'],
                'time'=>$request['time'],
                'math'=>$request['math'],
                'onlinePersonal'=>$request['onlinePersonal'],
                'onlineGroup'=>$request['onlineGroup'],
                'offlinePersonal'=>$request['offlinePersonal'],
                'offlineGroup'=>$request['offlineGroup'],
                'avatar'=>'lesson/'.$imagename,
            ]);

            return redirect()->route('admin.lesson.index');
        }else{
            LessonModels::find($id)->update([
                'slug'=>$request['slug'],
                'title'=>$request['title'],
                'language'=>$request['language'],
                'description'=>$request['description'],
                'status'=>$request['status'],
                'time'=>$request['time'],
                'math'=>$request['math'],
                'onlinePersonal'=>$request['onlinePersonal'],
                'onlineGroup'=>$request['onlineGroup'],
                'offlinePersonal'=>$request['offlinePersonal'],
                'offlineGroup'=>$request['offlineGroup'],

            ]);
            return redirect()->route('admin.lesson.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson=LessonModels::find($id)->delete();
        return  back();

    }
}
