<?php

namespace App\Http\Controllers\Admin\Lesson;

use App\Models\Lesson\LessonTrainersModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class LessonTrainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainer=LessonTrainersModels::get();

        return view('admin.Lesson.trainers.index',compact('trainer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Lesson.trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('Trainers/');
            $pah = public_path('Trainers/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            LessonTrainersModels::create([
                'name'=>$request['name'],
                'profession'=>$request['profession'],
                'avatar'=>'Trainers/'.$imagename,
            ]);

            return redirect()->route('admin.trainers.index');
        }else{

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trainer=LessonTrainersModels::find($id);

        return view('admin.Lesson.trainers.edit',compact('trainer','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('Trainers/');
            $pah = public_path('Trainers/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            LessonTrainersModels::find($id)->update([
                'name'=>$request['name'],
                'profession'=>$request['profession'],
                'avatar'=>'Trainers/'.$imagename,
            ]);

            return redirect()->route('admin.trainers.index');
        }else{
            LessonTrainersModels::find($id)->update([

                'name'=>$request['name'],
                'profession'=>$request['profession'],

            ]);
            return redirect()->route('admin.trainers.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trainer=LessonTrainersModels::find($id)->delete();
        return back();
    }
}
