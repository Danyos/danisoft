<?php

namespace App\Http\Controllers\Admin\Lesson;

use App\Models\Lesson\LessonPriceListModels;
use App\Models\Lesson\LessonTrainersModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LessonPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessonPrice=LessonPriceListModels::get();

        return view('admin.Lesson.lessonPrice.index',compact('lessonPrice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Lesson.lessonPrice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        LessonPriceListModels::create($request->all());
        return redirect()->route('admin.lessonPrice.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lessonPrice=LessonPriceListModels::find($id);

        return view('admin.Lesson.lessonPrice.edit',compact('lessonPrice','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lessonPrice=LessonPriceListModels::find($id)->update($request->all());
        return redirect()->route('admin.lessonPrice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lessonPrice=LessonPriceListModels::find($id)->delete();
        return  back();
    }
}
