<?php

namespace App\Http\Controllers\Admin\Lesson;

use App\Models\Lesson\LessonInformateModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LessonSpecialInformateModels extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.Lesson.special.info.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specialInform=LessonInformateModels::create($request->all());

        return redirect()->route('admin.specialInform.show',$request->parent_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $specialInform=LessonInformateModels::where('parent_id',$id)->get();

        return view('admin.Lesson.special.info.index',compact('id','specialInform'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specialInform=LessonInformateModels::find($id);

        return view('admin.Lesson.special.info.edit',compact('id','specialInform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specialInform=LessonInformateModels::find($id)->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specialInform=LessonInformateModels::find($id)->delete();
        return back();
    }
}
