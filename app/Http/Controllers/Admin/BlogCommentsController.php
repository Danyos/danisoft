<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogCommentsModels;
use App\Models\BlogModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminControoler;
use Illuminate\Support\Facades\Auth;

class BlogCommentsController extends AdminControoler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $blog=BlogCommentsModels::create([
          'blog_id'=>$request->blog,
          'user_id'=>Auth::id(),
          'message'=>$request->msg,
      ]);
        return view('admin.blog.comment.comments',compact('blog'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blognews=BlogModels::find($id);
        $blog=BlogCommentsModels::where('blog_id',$id)->orderby('id','desc')->get();
        return view('admin.blog.comment.index',compact('blog','id','blognews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request)
    {
   $blog=BlogCommentsModels::find($request->id)->delete();


        return $request->id;

    }
}
