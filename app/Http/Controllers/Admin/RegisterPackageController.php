<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lesson\RegisterLessonModel;
use App\Models\PackageModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterPackageController extends Controller
{
    public function lesson(){
        $register=RegisterLessonModel::orderBy('id','desc')->get();
        $register_active=RegisterLessonModel::where('status','inactive')->get()->count();
        return view('admin.Lesson.register.index',compact('register','register_active'));
    }
    public function lessonUpdate($id){
        $regiser=RegisterLessonModel::find($id)->update(['status'=>'active']);
        return back();
    }
    public function service(){
        $register=\App\Models\PackageModels::get();
        return view('admin.service.register.index',compact('register'));
    }
    public function serviceUpdate($id){
        $regiser=PackageModels::find($id)->update(['status'=>'active']);
        return back();
    }
}
