<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServiceInformateModels;
use App\Models\ServiceModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service=ServiceModels::with('serviceMain')->get();

        return view('admin.service.index', compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('service/');
            $pah = public_path('service/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            ServiceModels::create([
                'title'=>$request['title'],
                'description'=>$request['description'],
                'offer'=>$request['offer'],
                'url'=>$request['url'],
                'avatar'=>'service/'.$imagename,
            ]);

            return redirect()->route('admin.service.index');
        }else{

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service=ServiceModels::find($id);
        $service_informate=ServiceInformateModels::where('parent_id',$id)->get();

        return view('admin.service.show', compact('service','service_informate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service=ServiceModels::find($id);
        return view('admin.service.edit', compact('service','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('avatar')) {

            $imagename = date("Y") . '/' . date("m") . '/' . rand(1, 99999) . '.' . $request->avatar->getClientOriginalExtension();

            $destinationPath = public_path('service/');
            $pah = public_path('service/' . date("Y") . '/' . date("m"));
            if (!file_exists($pah) > 0):
                if (!is_dir($pah . date("Y") . '/' . date("m"))) {
                    mkdir($pah, 0755, true);
                }endif;

            $thumb_img = Image::make($request->avatar->getRealPath())->encode('jpg', 75);
            $thumb_img->save($destinationPath . '/' . $imagename, 80);

            ServiceModels::find($id)->update([
                'title'=>$request['title'],
                'description'=>$request['description'],
                'offer'=>$request['offer'],
                'url'=>$request['url'],
                'avatar'=>'service/'.$imagename,
            ]);

            return back();
        }else{
            ServiceModels::find($id)->update([

                    'title'=>$request['title'],
                    'description'=>$request['description'],
                    'offer'=>$request['offer'],
                    'url'=>$request['url'],

                ]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceModels::find($id)->delete();
        return back();
    }
}
