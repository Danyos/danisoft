<?php

namespace App\Http\Controllers;

use App\Models\BlogCommentsModels;
use App\Models\BlogModels;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class Blog_Controller extends Controller
{
    public function blogitems()
    {
        $blogItems = BlogModels::orderBy('created_at', 'desc')->paginate(12);


        return view('page.blog.index', compact('blogItems'));
    }

    public function blogid($id)
    {
        $blogall = BlogModels::where('slug','!=',$id)->inRandomOrder()->paginate(6);
        $blogItems = BlogModels::where('slug',$id)->first();
        $blogComment=BlogCommentsModels::where('blog_id',$id)->get();

        return view('page.blog.show', compact('blogItems', 'blogall','blogComment'));
    }

public function create(Request $request){

    try {
        $data= request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'blog_id' => 'required',
        ]);




        $Comments = BlogCommentsModels::create($request->all());
        $blogComments = BlogCommentsModels::find($Comments->id);


        return view('data.comments',compact('blogComments'));

    }catch (\Exception $exception){
        return 'error';
    }




}

}
