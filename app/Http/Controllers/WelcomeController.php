<?php

namespace App\Http\Controllers;

use App\Models\AboutInformateModels;
use App\Models\AboutParentModels;
use App\Models\BlogModels;
use App\Models\ContactModel;
use App\Models\Lesson\LessonInformateModels;
use App\Models\Lesson\LessonModels;
use App\Models\Lesson\LessonParentModels;
use App\Models\Lesson\LessonPriceListModels;
use App\Models\Lesson\LessonTrainersIdGroupsModels;
use App\Models\Lesson\LessonTrainersModels;
use App\Models\PackageModels;
use App\Models\ServiceInformateModels;
use App\Models\ServiceModels;
use App\Models\SliderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class WelcomeController extends Controller
{
    public function index()
    {
        $aboutParent = AboutParentModels::first();
        $aboutinformate = AboutInformateModels::orderBy('id', 'desc')->get();
        // $blogItems = BlogModels::where('home', 'active')->get();
        // $blogItems = BlogModels::orderBy('id','desc')->get()->take(6);
        $slider = SliderModel::orderBy('id', 'desc')->get();
        return view('welcome', compact( 'aboutParent', 'aboutinformate',  'slider'));
    }

    public function abouts()
    {

        $aboutParent = AboutParentModels::first();
        $aboutinformate = AboutInformateModels::orderBy('id', 'desc')->get();
        return view('page.about', compact('aboutParent', 'aboutinformate'));
    }

    public function school()
    {

        $aboutParent = AboutParentModels::first();
        $aboutinformate = AboutInformateModels::orderBy('id', 'desc')->get();
        return view('page.school.school', compact('aboutParent', 'aboutinformate'));
    }

    public function service($service_id = '')
    {

        if ($service_id) {

            $lastservice = ServiceModels::where('slug',$service_id)->first();
        } else {

            $lastservice = ServiceModels::first();
        }
        $service_ids = $lastservice->id;
        $lesson=LessonModels::where('status','active')->get();
        $service_informate = ServiceInformateModels::where('parent_id', $service_ids)->get();


        return view('page.service', compact('lastservice', 'service_id', 'lesson','service_informate'));
    }

    public function lessonregister($data=' ')
    {


        return view('page.school.register',compact('data'));
    }

    public function lesson()
    {

        $lesson=LessonModels::where('status','active')->get();

        return view('page.school.lesson',compact('lesson'));
    }
    public function special($data)
    {

        $lesson=LessonParentModels::where('data',$data)->get();

        return view('page.school.special',compact('lesson'));
    }

    public function lessonshow($less)
    {
        $lesson=LessonParentModels::where('slug',$less)->first();
        $pricedata=LessonPriceListModels::find($lesson->price_id);
        $lessonInformate=LessonInformateModels::where('parent_id',$lesson->id)->get();
$grup=LessonTrainersIdGroupsModels::where('lesson_id',$lesson->id)->get('trainers_id')->toArray();
$trainers=LessonTrainersModels::whereIn('id',$grup)->get();
        return view('page.school.show',compact('lesson','lessonInformate','trainers','pricedata'));
    }

    public function ourworks()
    {


        return view('page.ourwork');
    }


    public function contact()
    {

        return view('page.contact');
    }

    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:120',
            'email' => 'required|email',
            'tel' => 'required|min:11|numeric',
            'message' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        ContactModel::create($request->all());

        return back()->with('succsess', 'Your message in send');
    }
    public function packages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'tel' => 'required',
            'package' => 'required',
            'parent_id' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();

        }

        $check = PackageModels::create($request->all());


        return back()->with('succsess', 'Your message in send');
    }






}
