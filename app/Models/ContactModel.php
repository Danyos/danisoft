<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    protected $fillable = [
        'name',
        'email',
        'tel',
        'message',
        'status'
    ];
}
