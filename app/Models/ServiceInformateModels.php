<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInformateModels extends Model
{
    protected $fillable = [
        'title',
        'price',
        'description',
        'href',
        'parent_id',
    ];
    public function serviceMain()
    {
        return $this->hasMany(ServicePackage::class,'child_id','id');
    }
}
