<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentsModels extends Model
{
    protected $fillable = [
        'name',
        'email',
        'blog_id',
        'message',
        'avatar',
    ];
}
