<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonParentModels extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'data',
        'description',
        'avatar',
        'logo',
        'price_id',
        'slogan',
    ];
}
