<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonTrainersModels extends Model
{
    protected $fillable = [
        'name',
        'profession',
        'avatar',
    ];
}
