<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonTrainersIdGroupsModels extends Model
{
    protected $fillable = [
        'lesson_id',
        'trainers_id',

    ];
}
