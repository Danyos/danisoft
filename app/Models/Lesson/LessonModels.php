<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonModels extends Model
{
    protected $fillable = [
        'slug',
        'title',
        'language',
        'description',
        'avatar',
        'status',
        'time',
        'math',
        'onlinePersonal',
        'onlineGroup',
        'offlinePersonal',
        'offlineGroup',
    ];
}
