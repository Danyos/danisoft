<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class RegisterLessonModel extends Model
{
    protected $fillable = [
        'name',
        'age',
        'email',
        'tel',
        'slug',
        'status',

    ];
}
