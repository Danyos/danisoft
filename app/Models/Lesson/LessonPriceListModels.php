<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonPriceListModels extends Model
{
    protected $fillable = [
        'math',
        'time',
        'onlinePersonal',
        'onlineGroup',
        'offlinePersonal',
        'offlineGroup',
    ];
}
