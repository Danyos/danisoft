<?php

namespace App\Models\Lesson;

use Illuminate\Database\Eloquent\Model;

class LessonInformateModels extends Model
{
    protected $fillable = [
        'parent_id',
        'title',

        'description',

    ];
}
