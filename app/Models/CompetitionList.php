<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CompetitionList extends Model
{

    protected $fillable=[
        'parent_id','to'
    ];

    public function ManInfo(){
        return $this->hasOne(CompetitionModel::class,'id','parent_id');
    }
}
