<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionModel extends Model
{
    protected $fillable=[
        'name','link','avatarLink'
    ];
}
