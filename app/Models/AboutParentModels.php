<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutParentModels extends Model
{
    protected $fillable = [

        'title',
        'description',
        'avatar',
        'url',
    ];
}
