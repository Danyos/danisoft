<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageModels extends Model
{
    protected $fillable = [
        'name',
        'package',
        'parent_id',
        'tel',
        'status',

    ];
}
