<?php

Route::get('/', 'WelcomeController@index')->name('index');
Route::get('/aboutUs', 'WelcomeController@abouts')->name('about');
Route::get('{id?}-and-pricing', 'WelcomeController@service')->name('service');
Route::get('/our-Work', 'WelcomeController@ourworks')->name('our.work');
Route::get('/web/school', 'WelcomeController@school')->name('school.lesson');
Route::get('/lesson-register/{data?}', 'WelcomeController@lessonregister')->name('lesson.register');
Route::post('/register/lesson', 'Lesson_registration_Controller@store')->name('lessonRegister.store');
Route::get('web/lesson', 'WelcomeController@lesson')->name('lesson');
Route::get('special/{data}', 'WelcomeController@special')->name('special');
Route::get('show-{lesson}', 'WelcomeController@lessonshow')->name('lesson.show');

Route::get('/blog/news', 'Blog_Controller@blogitems')->name('blognews');
Route::get('/blog/{id}', 'Blog_Controller@blogid')->name('blogid');
Route::post('/blog/created', 'Blog_Controller@create')->name('blog.created');
Route::get('/contact', 'WelcomeController@contact')->name('contact');
Route::post('/send', 'WelcomeController@send')->name('send');
Route::post('/package', 'WelcomeController@packages')->name('package');
Auth::routes(['register' => false]);
Route::redirect('/home', '/admin');



Route::group([ 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::resource('campetition', 'CampetitionController');
    Route::get('ajaxRequest/{id}', 'CampetitionController@ajaxRequest');
    Route::get('result', 'CampetitionController@result')->name('results');
    Route::get('trashCache', 'CampetitionController@trashCache')->name('trashCache');

});
    Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/contact', 'ContactController');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');
    Route::resource('slider', 'SliderController');
    Route::resource('about', 'AboutParentController');
    Route::resource('aboutinform', 'AboutInformController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');
    Route::resource('blog', 'BlogController');
    Route::get('blog/chek/slug', 'BlogController@chek')->name('chek.slug');
    Route::resource('service', 'ServiceController');
    Route::resource('service', 'ServiceController');
    Route::get('service/view/{id}', 'ServiceInformateController@view')->name('service_info.view');
    Route::get('service/add/{id}', 'ServiceInformateController@create')->name('service_info.created');
    Route::resource('service_info', 'ServiceInformateController');
    Route::resource('service_Package', 'ServicePackage');
    Route::get('service/Package/{id}', 'ServicePackage@creates')->name('ChildservicesPackage');
    Route::resource('bcomments', 'BlogCommentsController');
    Route::post('bcomments/delete', 'BlogCommentsController@delete')->name('comment.delete');


//    Lesson start


        Route::resource('lesson', 'Lesson\LessonController');
        Route::resource('trainers', 'Lesson\LessonTrainersController');
        Route::resource('lessonPrice', 'Lesson\LessonPriceController');
        Route::resource('special', 'Lesson\LessonSpecialModels');
        Route::resource('specialInform', 'Lesson\LessonSpecialInformateModels');
        Route::get('specialInforms/{data}', 'Lesson\LessonSpecialInformateModels@create')->name('create.special');
        Route::get('packageLesson', 'RegisterPackageController@lesson')->name('package.Lesson');
        Route::get('packageLessons/{id}', 'RegisterPackageController@lessonUpdate')->name('package.Lessons');

        Route::get('packageService', 'RegisterPackageController@service')->name('package.Service');
        Route::get('packageService/{id}', 'RegisterPackageController@serviceUpdate')->name('package.Services');







});
